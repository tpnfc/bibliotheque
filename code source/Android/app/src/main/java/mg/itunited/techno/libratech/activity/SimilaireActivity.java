package mg.itunited.techno.libratech.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import mg.itunited.techno.libratech.R;
import mg.itunited.techno.libratech.adapter.CategoBookAdapteur;
import mg.itunited.techno.libratech.model.BookItem;
import mg.itunited.techno.libratech.utils.ConnectionDetector;
import mg.itunited.techno.libratech.utils.VolleyApplication;

public class SimilaireActivity extends AppCompatActivity {

    private ListView listSimilaire;
    private CategoBookAdapteur adapter;
    private Button btnRetour;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_catego);

        Intent intent =getIntent();
        ArrayList<BookItem> books = intent.getExtras().getParcelableArrayList("similaire");

        listSimilaire = (ListView) findViewById(R.id.lv_recherche_catego);

        adapter = new CategoBookAdapteur(getApplicationContext(), R.layout.list_recherche_row,books);
        listSimilaire.setAdapter(adapter);
        listSimilaire.setOnItemClickListener(new SimilaireListener());

        btnRetour = (Button) findViewById(R.id.button_recherche_retour);
        btnRetour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }

    private class SimilaireListener implements AdapterView.OnItemClickListener{
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            BookItem item = (BookItem) parent.getItemAtPosition(position);
            Intent intent = new Intent(view.getContext(), SearchDetailActivity.class);
            intent.putExtra("detail",item);
            startActivity(intent);

        }
    }










}
