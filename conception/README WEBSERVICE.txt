
livres empruntes par utilisateur 
requete: GET
url: http://ocean-cash.com/bibliotheque/api/webservice/emprunts
parametre: UTILISATEURSID
exemple: http://ocean-cash.com/bibliotheque/api/webservice/emprunts?UTILISATEURSID=6



recuperation de tous les livres
requete: GET
url: http://ocean-cash.com/bibliotheque/api/webservice/livre
parametre: 


recuperation des livres PAR CATEGORIE
requete: GET
url: http://ocean-cash.com/bibliotheque/api/webservice/livre
parametre: IDCATEGORIE
exemple: http://ocean-cash.com/bibliotheque/api/webservice/livre?IDCATEGORIE=3


recuperation livre similaire par BOOKID
requete:GET
url: http://ocean-cash.com/bibliotheque/api/webservice/livre_similaire
parametre: BOOKID
exemple: http://ocean-cash.com/bibliotheque/api/webservice/livre_similaire?BOOKID=6


ajout livre empruntees
requete:POST
url:http://ocean-cash.com/bibliotheque/api/webservice/emprunts
parametre: 'UTILISATEURSID',
            'BOOKID' ,
            'DATEEMPRUNTS',
            'DATERESTITUTION' 
			
login utilisateur
requete: POST
url:http://ocean-cash.com/bibliotheque/api/webservice/login
parametre: login,pass
			
			
inscription utilisateur
requete: POST
url:http://ocean-cash.com/bibliotheque/api/webservice/inscription
parametre: login,mdp,email,nom,prenom


compter livres empruntees par utilisateur
requete:GET
url:http://ocean-cash.com/bibliotheque/api/webservice/count_emprunts
parametre: UTILISATEURSID
ex retour: 2

RETOUR LIVRES
requete: GET
url:http://ocean-cash.com/bibliotheque/api/webservice/retour
parametre: UTILISATEURSID,BOOKID
ex retour:{
   "status": true,
   "message": "Livre rendu avec succees "
}
