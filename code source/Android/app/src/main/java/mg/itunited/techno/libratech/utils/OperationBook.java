package mg.itunited.techno.libratech.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import mg.itunited.techno.libratech.model.BookItem;

/**
 * Created by Andrianina on 14/04/2016.
 */
public class OperationBook {

    public  static String urlEmprunt="http://ocean-cash.com/bibliotheque/api/webservice/emprunts";
    public  static String urlSimilaire="http://ocean-cash.com/bibliotheque/api/webservice/livre_similaire?BOOKID=";
    public  static String urlRetour="http://ocean-cash.com/bibliotheque/api/webservice/retour";
    private Map<String,String> params = new HashMap<String, String>();
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
    private ConnectionDetector connectionDetector;
    private ArrayList<BookItem> result = new ArrayList<BookItem>();
    private Context mycontext;


    public void EmprunterLivre(String iduser, String bookid , Context context, final VolleyCallback callback){

        mycontext = context;
        connectionDetector = new ConnectionDetector(context);

        Date date =new Date();
        String curentdate = simpleDateFormat.format(date);
        String nextdate = simpleDateFormat.format(addTenDays(date));

        params.put("UTILISATEURSID",iduser);
        params.put("BOOKID",bookid);
        params.put("DATEEMPRUNTS",curentdate);
        params.put("DATERESTITUTION",nextdate);

        StringRequest request =new StringRequest(Request.Method.POST, urlEmprunt, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Toast.makeText(mycontext, "Emprunt effectué", Toast.LENGTH_LONG).show();
                callback.onSuccess(true);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if(!connectionDetector.isConnectingToInternet()){

                    Toast.makeText(mycontext, " Aucune connexion à Internet", Toast.LENGTH_LONG).show();
                }else {
                    Toast.makeText(mycontext, " Erreur de connexion", Toast.LENGTH_LONG).show();
                }
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return params;
            }
        };
        VolleyApplication.getInstance().addToRequestQueue(request);
    }


    public void SimilaireLivre(String bookid, Context context, final VolleyCallback callback){

        mycontext = context;
        connectionDetector = new ConnectionDetector(context);


        JsonArrayRequest request =new JsonArrayRequest(urlSimilaire+bookid, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                try {
                    ArrayList<BookItem> bookItems = parseSimilaire(response);
                    Toast.makeText(mycontext, " Livre similaire obtenu ="+bookItems.size(), Toast.LENGTH_LONG).show();
                    callback.onDataFetch(true,bookItems);


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if(!connectionDetector.isConnectingToInternet()){

                    Toast.makeText(mycontext, " Aucune connexion à Internet", Toast.LENGTH_LONG).show();
                }else {
                    Toast.makeText(mycontext, " Erreur lors de la connexion au serveur ", Toast.LENGTH_SHORT).show();
                }
            }
        }
        );
        VolleyApplication.getInstance().addToRequestQueue(request);
    }



    public void RetourLivre(String iduser, String bookid, Context context, final VolleyCallback callback){

        mycontext = context;
        connectionDetector = new ConnectionDetector(context);


        String requete = urlRetour+"?UTILISATEURSID="+iduser+"&BOOKID="+bookid;

        StringRequest request =new StringRequest(requete, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Toast.makeText(mycontext, "Retour effectué", Toast.LENGTH_LONG).show();
                callback.onSuccess(true);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if(!connectionDetector.isConnectingToInternet()){

                    Toast.makeText(mycontext, " Aucune connexion à Internet", Toast.LENGTH_LONG).show();
                }else {
                    Toast.makeText(mycontext, " Erreur de connexion", Toast.LENGTH_LONG).show();
                }
            }
        });
        VolleyApplication.getInstance().addToRequestQueue(request);

    }


    public Date addTenDays(Date current){

        Calendar cal = Calendar.getInstance();
        cal.setTime(current);
        cal.add(Calendar.DATE, 10); // add 10 days
        return cal.getTime();

    }



    private ArrayList<BookItem> parseSimilaire(JSONArray json)throws JSONException {
        ArrayList<BookItem> all =new ArrayList<BookItem>();

        for(int i=0; i< json.length();i++){

            BookItem newItem= new BookItem();
            JSONObject jsonItem = json.getJSONObject(i);
            newItem.setBOOKID(jsonItem.getInt("BOOKID"));
            newItem.setIDCATEGORIE(jsonItem.getInt("IDCATEGORIE"));
            newItem.setTITRE(jsonItem.getString("TITRE"));
            newItem.setAUTEUR(jsonItem.getString("AUTEUR"));
            newItem.setRESUME(jsonItem.getString("RESUME"));
            newItem.setEDITEUR(jsonItem.getString("EDITEUR"));
            newItem.setCATEGORIE(jsonItem.getString("INTITULECATEGORIE"));
            newItem.setETAT(jsonItem.getInt("ETAT"));
            all.add(newItem);

        }

        return all;
    }



    public interface VolleyCallback{
         void onSuccess(boolean state);
         void onDataFetch(boolean state,ArrayList<BookItem> bookItems);
    }


}
