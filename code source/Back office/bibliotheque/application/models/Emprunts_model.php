<?php
/**
 * Created by PhpStorm.
 * User: Fanantenana
 * Date: 11/03/16
 * Time: 13:19
 */

class Emprunts_model  extends CI_Model {

    protected $table = 'emprunts';

    function insert($data)
    {
        return $this->db
            ->insert($this->table, $data);
    }

    public function emprunt_upd($bookid)
    {
        $this->db->where('BOOKID', $bookid);
        $this->db->update('livres', array('ETAT' => 0));
    }

} 