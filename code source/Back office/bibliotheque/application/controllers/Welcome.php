<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {



    function __construct()
    {
        parent::__construct();
        $this->load->library('pagination');
        $this->load->model('Livre_model','livre_model');

    }


    public function index()
	{
        $data['nb_livres']=$this->livre_model->count_livres();
        $data['nb_livres_dispo']=$this->livre_model->ccount_livres_dispo();
        $data['nb_livres_empruntes']=$this->livre_model->ccount_livres_empruntes();
		$this->load->view('index',$data);
	}
}
