package mg.itunited.techno.libratech.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import mg.itunited.techno.libratech.R;
import mg.itunited.techno.libratech.adapter.BookListAdapter;
import mg.itunited.techno.libratech.adapter.CategoAdapteur;
import mg.itunited.techno.libratech.adapter.CategoBookAdapteur;
import mg.itunited.techno.libratech.model.BookItem;
import mg.itunited.techno.libratech.utils.ConnectionDetector;
import mg.itunited.techno.libratech.utils.VolleyApplication;

public class CategoActivity extends AppCompatActivity {

    private ListView listCatego;
    private CategoBookAdapteur adapter;
    private Button btnRetour;
    private ProgressDialog pDialog;
    private ConnectionDetector connectionDetector;
    private static String jsonToFetch = "http://ocean-cash.com/bibliotheque/api/webservice/livre?IDCATEGORIE=";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_catego);

        Intent intent =getIntent();
        String catego = intent.getExtras().getString("catego");



        connectionDetector = new ConnectionDetector(getApplicationContext());

        ArrayList<BookItem> books = new ArrayList<>();

        listCatego = (ListView) findViewById(R.id.lv_recherche_catego);

        adapter = new CategoBookAdapteur(getApplicationContext(), R.layout.list_recherche_row,books);
        listCatego.setAdapter(adapter);
        listCatego.setOnItemClickListener(new CategoListener());

        btnRetour = (Button) findViewById(R.id.button_recherche_retour);
        btnRetour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });





        fetch(catego);

        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading...");
        pDialog.show();



    }


    public void fetch(String catego){
        JsonArrayRequest request =new JsonArrayRequest(jsonToFetch+catego,new MyJsonListener(),new MyJsonErrorListener());
        VolleyApplication.getInstance().addToRequestQueue(request);
    }


    private class MyJsonListener implements Response.Listener<JSONArray> {

        @Override
        public void onResponse(JSONArray response) {
            try {
                ArrayList<BookItem> bookItems = parse(response);

                adapter.swapBookRecords(bookItems);
                pDialog.dismiss();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private class MyJsonErrorListener implements Response.ErrorListener{
        @Override
        public void onErrorResponse(VolleyError volleyError) {
            if(!connectionDetector.isConnectingToInternet()){

                Toast.makeText(getApplicationContext(), " Aucune connexion à Internet", Toast.LENGTH_LONG).show();
            }else {
                pDialog.dismiss();
                Toast.makeText(getApplicationContext(), " Erreur lors de la connexion au serveur ", Toast.LENGTH_SHORT).show();
            }
        }
    }


    private ArrayList<BookItem> parse(JSONArray json)throws JSONException{
        ArrayList<BookItem> all =new ArrayList<BookItem>();

        for(int i=0; i< json.length();i++){
            JSONObject jsonItem = json.getJSONObject(i);
            BookItem newItem= new BookItem();
            newItem.setBOOKID(jsonItem.getInt("BOOKID"));
            newItem.setIDCATEGORIE(jsonItem.getInt("IDCATEGORIE"));
            newItem.setTITRE(jsonItem.getString("TITRE"));
            newItem.setAUTEUR(jsonItem.getString("AUTEUR"));
            newItem.setRESUME(jsonItem.getString("RESUME"));
            newItem.setEDITEUR(jsonItem.getString("EDITEUR"));
            newItem.setCATEGORIE(jsonItem.getString("INTITULECATEGORIE"));
            newItem.setETAT(jsonItem.getInt("ETAT"));
            all.add(newItem);
        }

        return all;
    }



    private class CategoListener implements AdapterView.OnItemClickListener{
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            BookItem item = (BookItem) parent.getItemAtPosition(position);
            Intent intent = new Intent(view.getContext(), SearchDetailActivity.class);
            intent.putExtra("detail",item);
            startActivity(intent);

        }
    }



}
