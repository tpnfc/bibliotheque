package mg.itunited.techno.libratech.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.w3c.dom.Text;

import mg.itunited.techno.libratech.R;
import mg.itunited.techno.libratech.model.BookItem;

public class BookDetailActivity extends AppCompatActivity {

    private TextView txtTitre;
    private TextView txtCatego;
    private TextView txtEditeur;
    private TextView txtAuteur;
    private TextView txtEmprunt;
    private TextView txtRestitution;
    private TextView txtResume;
    private Button btnRetour;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_detail);

        Intent intent =getIntent();
        BookItem book = intent.getExtras().getParcelable("detail");

        btnRetour = (Button) findViewById(R.id.button_detail_retour);
        txtTitre = (TextView)findViewById(R.id.detail_titre);
        txtCatego = (TextView)findViewById(R.id.detail_categorie);
        txtEditeur = (TextView)findViewById(R.id.detail_editeur);
        txtAuteur = (TextView)findViewById(R.id.detail_auteur);
        txtEmprunt = (TextView)findViewById(R.id.detail_emprunt);
        txtRestitution = (TextView)findViewById(R.id.detail_restitution);
        txtResume = (TextView)findViewById(R.id.detail_resume);

        txtTitre.setText(book.getTITRE());
        txtCatego.setText(book.getCATEGORIE());
        txtEditeur.setText(book.getEDITEUR());
        txtAuteur.setText(book.getAUTEUR());
        txtEmprunt.setText(book.getDEBUT_EMPRUNT());
        txtRestitution.setText(book.getRESTITUTION_EMPRUNT());
        txtResume.setText(book.getRESUME());

        btnRetour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }
}
