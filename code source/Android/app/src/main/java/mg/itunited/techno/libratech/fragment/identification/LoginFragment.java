package mg.itunited.techno.libratech.fragment.identification;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import mg.itunited.techno.libratech.R;
import mg.itunited.techno.libratech.activity.IdentificationActivity;
import mg.itunited.techno.libratech.activity.MainActivity;
import mg.itunited.techno.libratech.model.BookItem;
import mg.itunited.techno.libratech.model.UserItem;
import mg.itunited.techno.libratech.utils.ConnectionDetector;
import mg.itunited.techno.libratech.utils.CustomRequest;
import mg.itunited.techno.libratech.utils.VolleyApplication;

/**
 * A placeholder fragment containing a simple view.
 */
public class LoginFragment extends Fragment {

    private Map<String,String> params =new HashMap<String,String>();
    private ConnectionDetector connectionDetector;
    private Button btnConnexion;
    private EditText editLogin;
    private EditText editPass;
    private TextView txtInscription;
    private ProgressDialog pDialog;
    private UserItem userItem = null;
    private String LOGIN;
    private String PASS;
    private String jsonToFetch ="http://ocean-cash.com/bibliotheque/api/webservice/login";

    public LoginFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

       connectionDetector = new ConnectionDetector(getContext());

        View rootView = inflater.inflate(R.layout.fragment_login, container, false);

        btnConnexion = (Button) rootView.findViewById(R.id.button_connexion);
        txtInscription = (TextView) rootView.findViewById(R.id.txt_inscription);
        editLogin = (EditText) rootView.findViewById(R.id.editText_login);
        editPass = (EditText) rootView.findViewById(R.id.editText_password);

        btnConnexion.setOnClickListener(new ConnexionListener());

        txtInscription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((IdentificationActivity)getActivity()).displayView(1);
            }
        });



        return rootView;
    }


    public void fetch(){
        CustomRequest request =new CustomRequest(Request.Method.POST,jsonToFetch,params,new MyJsonListener(),new MyJsonErrorListener());

        VolleyApplication.getInstance().addToRequestQueue(request);
    }

    private class MyJsonListener implements Response.Listener<JSONArray> {

        @Override
        public void onResponse(JSONArray response) {
            try {

                userItem = parse(response);
                System.out.println("----------------------------------->"+userItem.getUTILISATEURSID()+" - "+userItem.getNOM()+" - "+userItem.getPRENOM()+" - "+userItem.getMAIL());
                pDialog.dismiss();
                SharedPreferences prefs =
                        PreferenceManager.getDefaultSharedPreferences(
                                getContext());
                SharedPreferences.Editor editor = prefs.edit();
                editor.putInt("iduser",userItem.getUTILISATEURSID());
                editor.putString("nom",userItem.getNOM());
                editor.putString("prenom",userItem.getPRENOM());
                editor.commit();
                Toast.makeText(getContext(), " Bienvenue "+userItem.getPRENOM()+" !", Toast.LENGTH_LONG).show();

                Intent intent = new Intent(getContext(),MainActivity.class);
                startActivity(intent);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private class MyJsonErrorListener implements Response.ErrorListener{
        @Override
        public void onErrorResponse(VolleyError volleyError) {
            if(!connectionDetector.isConnectingToInternet()){
                pDialog.dismiss();
                Toast.makeText(getContext(), " Aucune connexion à Internet", Toast.LENGTH_LONG).show();
            }else{
                pDialog.dismiss();
                Toast.makeText(getContext(), " Login ou mot de passe incorrect ", Toast.LENGTH_SHORT).show();
            }

        }
    }

    private class ConnexionListener implements View.OnClickListener{
        @Override
        public void onClick(View v) {
            pDialog = new ProgressDialog(getContext());
            pDialog.setMessage("Loading...");
            pDialog.show();
            LOGIN = editLogin.getText().toString();
            PASS = editPass.getText().toString();

            params.put("login",LOGIN);
            params.put("pass",PASS);

            fetch();
        }
    }


    private UserItem parse(JSONArray json)throws JSONException{

            JSONObject jsonItem = json.getJSONObject(0);
            int id = jsonItem.getInt("UTILISATEURSID");
            String nom = jsonItem.getString("NOM");
            String prenom = jsonItem.getString("PRENOM");
            String mail = jsonItem.getString("MAIL");
            String login = jsonItem.getString("LOGIN");
            String mdp = jsonItem.getString("MDP");

        UserItem item = new UserItem(id,nom,prenom,mail,login,mdp);

        return item;
    }


}
