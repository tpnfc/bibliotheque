package mg.itunited.techno.libratech.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import mg.itunited.techno.libratech.R;
import mg.itunited.techno.libratech.adapter.BookListAdapter;
import mg.itunited.techno.libratech.model.BookItem;
import mg.itunited.techno.libratech.utils.ConnectionDetector;
import mg.itunited.techno.libratech.utils.VolleyApplication;

public class MyBookActivity extends AppCompatActivity {

    private ListView listBooks;
    private Button btnRetour;
    ProgressDialog pDialog;
    private ConnectionDetector connectionDetector;
    private BookListAdapter adapter = null;
    private String jsonToFetch = "http://ocean-cash.com/bibliotheque/api/webservice/emprunts";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_book);

        connectionDetector = new ConnectionDetector(getApplicationContext());

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        int iduser = prefs.getInt("iduser",0);

        ArrayList<BookItem> books = new ArrayList<BookItem>();




        listBooks = (ListView) findViewById(R.id.list_myBook);
        btnRetour = (Button) findViewById(R.id.btn_book_retour);
        adapter = new BookListAdapter(getApplicationContext(),R.layout.list_book_row,books);
        listBooks.setAdapter(adapter);
        listBooks.setOnItemClickListener(new EmpruntListener());

        btnRetour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent  = new Intent(getApplicationContext(),MainActivity.class);
                startActivity(intent);
                finish();
            }
        });



        jsonToFetch = jsonToFetch+"?UTILISATEURSID="+iduser;

        fetch();

        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading...");
        pDialog.show();

    }


    public void fetch(){
        JsonArrayRequest request =new JsonArrayRequest(jsonToFetch,new MyJsonListener(),new MyJsonErrorListener());
        VolleyApplication.getInstance().addToRequestQueue(request);
    }


    private class MyJsonListener implements Response.Listener<JSONArray> {

        @Override
        public void onResponse(JSONArray response) {
            try {
                ArrayList<BookItem> bookItems = parse(response);
                adapter.swapBookRecords(bookItems);
                pDialog.dismiss();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private class MyJsonErrorListener implements Response.ErrorListener{
        @Override
        public void onErrorResponse(VolleyError volleyError) {
            if(!connectionDetector.isConnectingToInternet()){

                Toast.makeText(getApplicationContext(), " Aucune connexion à Internet", Toast.LENGTH_LONG).show();
            }else {
            pDialog.dismiss();
            Toast.makeText(getApplicationContext(), " Erreur lors de la connexion au serveur ", Toast.LENGTH_SHORT).show();
            }
        }
    }


    private ArrayList<BookItem> parse(JSONArray json)throws JSONException{
        ArrayList<BookItem> all =new ArrayList<BookItem>();

        for(int i=0; i< json.length();i++){
            JSONObject jsonItem = json.getJSONObject(i);
            int id = jsonItem.getInt("BOOKID");
            int id_catgegorie = jsonItem.getInt("IDCATEGORIE");
            String titre = jsonItem.getString("TITRE");
            String auteur = jsonItem.getString("AUTEUR");
            String resume = jsonItem.getString("RESUME");
            String editeur = jsonItem.getString("EDITEUR");
            String categorie = jsonItem.getString("INTITULECATEGORIE");
            String debut_emprunt = jsonItem.getString("DATEEMPRUNTS");
            String restitution_emprunt = jsonItem.getString("DATERESTITUTION");
            int etat = jsonItem.getInt("ETAT");

            BookItem newItem = new BookItem(id,id_catgegorie,titre,auteur,resume,editeur,categorie,debut_emprunt,restitution_emprunt,etat);
            all.add(newItem);
        }

        return all;
    }


    private class EmpruntListener implements AdapterView.OnItemClickListener{
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            BookItem item = (BookItem) parent.getItemAtPosition(position);
            Intent intent = new Intent(view.getContext(), BookDetailActivity.class);
            intent.putExtra("detail",item);
            startActivity(intent);
        }
    }


}
