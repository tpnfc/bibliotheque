-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Jeu 14 Avril 2016 à 10:42
-- Version du serveur :  5.6.17
-- Version de PHP :  5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `bibliotheque_db`
--

-- --------------------------------------------------------

--
-- Structure de la table `categorie`
--

CREATE TABLE IF NOT EXISTS `categorie` (
  `IDCATEGORIE` int(11) NOT NULL AUTO_INCREMENT,
  `INTITULECATEGORIE` varchar(30) NOT NULL,
  PRIMARY KEY (`IDCATEGORIE`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

--
-- Contenu de la table `categorie`
--

INSERT INTO `categorie` (`IDCATEGORIE`, `INTITULECATEGORIE`) VALUES
(1, 'Bande dessine'),
(2, 'Cuisine'),
(3, 'Histoire'),
(4, 'Informatique, internet'),
(5, 'Humour'),
(6, 'Jeunesse'),
(7, 'Litterature'),
(8, 'Religion et spiritualite'),
(9, 'Romance'),
(10, 'Sciences humaines'),
(11, 'scolaire'),
(12, 'theatre'),
(13, 'tourisme et voyage'),
(14, 'Sport et loisirs'),
(15, 'Aventure');

-- --------------------------------------------------------

--
-- Structure de la table `emprunts`
--

CREATE TABLE IF NOT EXISTS `emprunts` (
  `IDEMPRUNTS` int(11) NOT NULL AUTO_INCREMENT,
  `UTILISATEURSID` int(11) NOT NULL,
  `BOOKID` int(11) NOT NULL,
  `DATEEMPRUNTS` date DEFAULT NULL,
  `DATERESTITUTION` date DEFAULT NULL,
  PRIMARY KEY (`IDEMPRUNTS`),
  KEY `CONCERNER_FK` (`BOOKID`),
  KEY `FAIRE_FK` (`UTILISATEURSID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Contenu de la table `emprunts`
--

INSERT INTO `emprunts` (`IDEMPRUNTS`, `UTILISATEURSID`, `BOOKID`, `DATEEMPRUNTS`, `DATERESTITUTION`) VALUES
(1, 2, 10, '2016-03-01', '2016-03-24'),
(2, 6, 5, '2016-03-11', '2016-03-19'),
(3, 6, 3, '2016-03-11', '2016-03-31'),
(6, 2, 34, '2016-03-09', '2016-03-31'),
(7, 2, 32, '2016-03-09', '2016-03-31'),
(8, 2, 30, '2016-03-08', '2016-03-31'),
(9, 2, 28, '2016-03-02', '2016-03-22'),
(10, 3, 27, '2016-03-01', '2016-03-31'),
(11, 4, 18, '2016-02-09', '2016-03-20'),
(12, 5, 13, '2016-03-01', '2016-03-31'),
(13, 5, 14, '2016-03-01', '2016-03-31');

-- --------------------------------------------------------

--
-- Structure de la table `livres`
--

CREATE TABLE IF NOT EXISTS `livres` (
  `BOOKID` int(11) NOT NULL AUTO_INCREMENT,
  `IDCATEGORIE` int(11) NOT NULL,
  `TITRE` varchar(80) NOT NULL,
  `AUTEUR` varchar(100) NOT NULL,
  `RESUME` text,
  `EDITEUR` varchar(80) NOT NULL,
  `ETAT` tinyint(1) NOT NULL DEFAULT '1',
  `UIID` varchar(255) NOT NULL,
  PRIMARY KEY (`BOOKID`),
  KEY `AVOIR_CATEGORIE_FK` (`IDCATEGORIE`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=39 ;

--
-- Contenu de la table `livres`
--

INSERT INTO `livres` (`BOOKID`, `IDCATEGORIE`, `TITRE`, `AUTEUR`, `RESUME`, `EDITEUR`, `ETAT`, `UIID`) VALUES
(3, 15, 'Franz Kafka, lE Proces', 'Clod', 'Le jour de son arrestation, K. ouvre la porte de sa chambre pour s''informer de son petit-déjeuner et amorce ainsi une dynamique du questionnement qui s''appuie, tout au long du roman, sur cette métaphore de la porte. Accusé d''une faute qu''il ignore par des juges qu''il ne voit jamais et conformément à des lois que personne ne peut lui enseigner, il va pousser un nombre ahurissant de portes pour tenter de démêler la situation. À mesure que le procès prend de l''ampleur ... ', ' Gallimard (1987) ', 0, ''),
(4, 2, 'Syndrome du colon irritable', 'Alexandra Leduc', 'Vous vous sentez souvent ballonné, votre ventre gargouille à n''en plus finir, votre transit est irrégulier... et vous avez tout essayé pour éviter ces troubles digestifs ? Découvrez une alimentation adaptée peut vous aider à combattre efficacement les symptômes et à réduire vos inconforts. Cette nouvelle édition, revue et augmentée, s''inspire de l''approche FODMAP, une méthode novatrice qui permet de soulager les symptômes gastro-intestinaux chez la plupart des personnes atteintes du côlon irritable.', ' Modus Vivendi ›', 1, ''),
(5, 2, 'Un goût de paradis : 60 recettes bienfaisantes pour le corps et pour l''esprit', 'Florence Pomana', 'Lors de son voyage en Inde en 1991, Florence Pomana a découvert un florilège de sensations nouvelles, des saveurs et une expérience gustative uniques. Depuis, elle n''a eu de cesse de chercher, d''apprendre et de créer, autour de cet art culinaire, si bienfaisant pour le corps et pour l''esprit, en totale harmonie avec la nature.', 'Editions de la Martinière ›', 0, ''),
(6, 1, 'space travelers', 'kazue kato', 'Seul habitant de Planetoïd, située au fin fond du système solaire, Robin est un jeune garçon-robot. Son quotidien est bouleversé le jour où un lapin humanoïde débarque sur sa planète. Celui-ci, prénommé Usa, s''avère être un évadé du pénitencier de Pluton poursuivi par la terrible armée de l''Alliance du Système Solaire.\r\nPour aider son nouvel ami, Robin se voit contraint de recourir à une mystérieuse vis rouge. Le voilà soudain possédé par une force maléfique et transformé en une arme redoutable, détruisant tout sur son passage ! Heureusement, Usa parvient à le délivrer en ôtant la vis logée dans son oeil gauche. Mais ils ne sont pas tirés d''affaire pour autant car l''armée découvre que le petit garçon n''est autre que «le fils des étoiles», qu''elle recherchait depuis des années.', ' Kazé Editions ›', 1, ''),
(7, 1, 'l''intelligence artificielle, fantasmes et réalités', 'Jean-Noël Lafargue', 'Jamais une science n''aura fait autant débat : alors que les "transhumanistes" comptent sur l''intelligence artificielle pour sauver l''espèce voire abolir la mort, Bill Gates ou Stephen Hawking affirment que l''avènement d''une entité informatique intelligente signera la perte de l''humanité ! Cette bande dessinée se penche à la fois sur l''histoire, la réalité et le fantasme de l''intelligence artificielle.', ' Lombard ›', 1, ''),
(8, 3, 'La Révolution française', 'Pascal Dupuy, Michel Biard', 'La Révolution française est ici revisitée à la lumière des recherches qui ont vu le jour depuis la célébration du Bicentenaire. L''ouvrage est ordonné selon un plan qui, tout en respectant le récit chronologique, insiste sur des aspects thématiques.\r\nDeux approches ont été privilégiées, sans pour autant que les autres soient négligées : d''une part, la Révolution est saisie comme une rupture politique majeure, fondamentale pour la construction de la démocratie en France ; d''autre part, elle est replacée dans son environnement international et le lecteur pourra en distinguer à la fois les singularités et les influences dans ce monde de la fin du XVIIIe siècle.\r\nDivisé en quatorze chapitres, l''ouvrage, outil de travail pour tous ceux qui s''intéressent à cette période, comporte un riche tableau chronologique et de très nombreuses références bibliographiques, notamment de livres publiés à l''étranger.', 'Armand Colin ›', 1, ''),
(9, 3, 'Les poilus d''orient', 'Pierre Miquel', 'De mars 1915 à mars 1919, des soldats français meurent en Orient. Aux Dardanelles d''abord, en combattant les Turcs encadrés par les Allemands. Mustafa Kemal y remporte la victoire sous les ordres de Liman von Sanders. A Salonique ensuite, où débarque un corps expéditionnaire pour le moins bigarré, comprenant des Britanniques et des Français bien sûr, mais aussi des Serbes chassés de leur pays, des Russes envoyés en renfort, des Siciliens et des Sardes, des Albanais, et, sur la fin, des Grecs.\r\nLes Sénégalais, les Marocains, les zouaves pieds-noirs, les marsouins meurent en première ligne au côté des joyeux des compagnies disciplinaires. A la fin de 1918, on expédie ces courageux en Roumanie pour tenir le front sud de la Russie contre les bolcheviks. Quand la flotte française de la mer Noire se mutine, ils sont enfin rapatriés.\r\nCeux que Clemenceau appelait avec mépris les " jardiniers de Salonique " auront donc fait la guerre cinq mois de plus que les autres.\r\nDécimés par les maladies autant que par la mitraille, commandés par des généraux écartés du théâtre des opérations en France, comme Sarrail et Franchet d''Esperey, les poilus d''Orient auront terriblement souffert de l''isolement moral sur un front mal ravitaillé.\r\nMais alors pourquoi, le moment venu, et en dépit de la réussite de leur percée sur le Danube, seront-ils les grands oubliés de la Victoire.\r\nCette épopée mal connue de la Première Guerre mondiale est ici racontée avec verve et passion par Pierre Miquel, dont les ouvrages sur la Grande Guerre font depuis longtemps référence.', 'Editions Tallandier ›', 1, ''),
(10, 4, 'Data Scientist et langage R', 'Henri Laude', 'Tous les experts s''accordent à dire que 90% des usages du Big Data proviennent de l''utilisation des data sciences. L''objectif de ce livre est de proposer une formation complète et opérationnelle sur les data-sciences qui permet de délivrer des solutions via l''usage du langage R.\r\nAinsi, l''auteur propose un parcours didactique et professionnalisant qui, sans autre pré-requis qu''un niveau Bac en mathématiques et une grande curiosité, permet aux lecteurs :\r\n- de s''intégrer à une équipe de data-scientists, - d''aborder des articles de recherche possédant une haute teneur en mathématiques, - le cas échéant de développer en langage R, y compris des algorithmes nouveaux et de beaux graphiques, - ou tout simplement de manager une équipe projet comprenant des data scientists, en étant à même de dialoguer avec eux de façon efficace.\r\nL''ouvrage ne se cantonne pas aux algorithmes du "machine learning", il aborde divers sujets importants comme le traitement du langage naturel, les séries temporelles, la logique floue, la manipulation des images.\r\nLa dynamique de l''ouvrage soutient le lecteur pas à pas dans sa découverte des data sciences, l''évolution de ses compétences théoriques et pratiques. Le praticien en exercice y découvrira également de nombreux savoir-faire à acquérir et le manager pourra surfer sur l''ouvrage après avoir lu attentivement le bestiaire des data-sciences de l''introduction, qui sans inexactitude ou vulgarisation excessive aborde le thème en faisant l''économie de mathématiques ou de formalismes dissuasifs.\r\nL''ensemble du code de tous les exemples de l''ouvrage et toutes les données traitées dans le code sont en téléchargement sur le site www.editions-eni.fr.', 'Editions ENI ›', 0, ''),
(11, 4, 'Swift pour les Nuls', 'Jesse Feiler', 'Swift est un nouveau langage de programmation performant et accessible qui permet de développer des apps iOS et Mac. Il est conçu pour offrir aux développeurs tous les éléments nécessaires à produire des apps. Swift est un langage simple d''approche et permet à ceux qui n''ont jamais utilisé un langage de programmation de coder après quelques heures seulement.\r\n\r\nAu programme :\r\nFaire tourner Swift dans l''environnement Xcode .\r\nDévelopper facilement des applications iOS et Mac .\r\nCollecter, déclarer et saisir des données .\r\nImplémenter des fonctionnalités de géolocalisation et de partage .', 'First Interactive ›', 1, ''),
(12, 5, 'Les petites âneries du permis de conduire', 'Didier Angheben, Franck Perria', 'Des perles comme celles-là, nos moniteurs en entendent tous les jours. Dans ce petit livre illustré, tendre et drôle, les auteurs, tous trois moniteurs d''auto-école, n''ont gardé que le meilleur du meilleur.\r\nAvec Les Petites âneries du permis de conduire nous pénétrons dans l''univers des moniteurs d''auto-école et de leurs étonnants élèves.\r\nOccasion pour chacun de revivre avec nostalgie et humour ce moment si important : le passage de son permis de conduire.', 'Les nouvelles éditions Françoi', 1, ''),
(13, 5, 'Les petites âneries du permis de conduire', 'Didier Angheben', 'Des perles comme celles-là, nos moniteurs en entendent tous les jours. Dans ce petit livre illustré, tendre et drôle, les auteurs, tous trois moniteurs d''auto-école, n''ont gardé que le meilleur du meilleur.\r\nAvec Les Petites âneries du permis de conduire nous pénétrons dans l''univers des moniteurs d''auto-école et de leurs étonnants élèves.\r\nOccasion pour chacun de revivre avec nostalgie et humour ce moment si important : le passage de son permis de conduire.', 'Les nouvelles éditions Françoi', 0, ''),
(14, 5, '150 idées pour emmerder le monde', 'Laurent Gaulet', 'Le 1er titre de la série à succès 150 idées pour emmerder. en version COLLECTOR !\r\n150 idées pour faire tourner en bourrique anonymes ou amis. Glisser un string dans le caddie d''une petite mamie juste avant qu''elle ne passe à la caisse, se garer en biais sur trois places sur le parking d''Auchan un samedi après-midi, tenir une conversation très intime dans le métro à l''heure de pointe.\r\n\r\nUn Petit livre essentiel pour sourire à ces idées sournoises ou s''en inspirer en cas de coup dur !', ' First ›', 1, ''),
(16, 6, 'Le renard et les poulettes', 'A. Cathala Kiko', 'Trois petites poules, devenues assez grandes pour vivre leur vie, s''en vont construire leur maison.\r\nElles ne se doutent pas qu''un renard vit juste à côté.\r\nUn renard qui adore les oeufs. Heureusement, les poulettes ont de la suite dans les idées.', ' Editions Milan ›', 1, ''),
(18, 7, 'La blancheur qu''on croyait éternelle', 'Virginie Carton', 'Mathilde et Lucien vivent à Paris, dans le même immeuble, mais ne se connaissent pas. Pourtant, ils se ressemblent.\r\nIl n''aime pas danser, elle n''aime pas se déguiser ; il aime les films avec Jean-Louis Trintignant, elle nourrit une passion secrète pour Romy Schneider ; il a gardé le ticket de métro du premier jour où il est allé au cinéma seul, elle a toujours dans son sac le foulard que lui a offert Julien.\r\nMais surtout, ils ont, depuis l''enfance, la même impression persistante de ne jamais se sentir à leur place nulle part. Ces deux sentimentaux parviendront-ils à se reconnaître dans une foule plus vraiment sentimentale ?', ' Le Livre de Poche ›', 0, ''),
(20, 7, 'Ce sont des choses qui arrivent', 'Pauline Dreyfus', '1945. Saint-Pierre-de-Chaillot, l''une des paroisses les plus huppées de Paris. L''aristocratie se presse pour enterrer la duchesse de Sorrente. Cette femme frivole et élégante a traversé la guerre d''une bien étrange façon. Elle portait en elle un secret. Les gens du monde l''ont partagé en silence. « Ce sont des choses qui arrivent », a-t-on murmuré avec indulgence.', 'Le Livre de Poche ›', 1, ''),
(22, 8, 'L origine de la pensée', 'Jiddu Krishnamurti', ' L''action juste n''est possible que lorsque l''esprit est silencieux, et qu''il s''opère une vision de «ce qui est». L''action qui découle de cette vision est débarrassée du passé, de la pensée et de la causalité. ».\r\nDans cette série de conférences inédites données en 1966 à Paris et Saanen, Krishnamurti rappelle que le chaos du monde n''est que la projection du chaos régnant dans chaque individu.\r\nLa pratique de la méditation peut opérer une profonde transformation de l''esprit. Quand le mental se calme, que l''esprit est dénué de « moi », sans vision ni images, il n''y a en lui plus de mémoire, plus de mouvement. Alors un intense foyer d''énergie se fait jour, creuset d''une réelle mutation.\r\nUn penseur d''une liberté et d''une envergure hors du commun. Plus que jamais d''actualité.', ' Presses du Châtelet ›', 1, ''),
(23, 8, 'Géopolitique des religions', 'Didier Giorgini', 'La géopolitique, au XXe siècle, a tenté de mettre en place de grands systèmes d''interprétation du monde, liés aux facteurs politiques, économiques et sociaux. Le XXIe siècle s''ouvre sur un coup de théâtre : il ne semble plus possible de comprendre le monde sans prendre en compte des aspects beaucoup moins quantifiables. Il s''agit du retour d''anciens « moteurs du monde » qu''on croyait grippés ou relégués en pièces de musée : les civilisations et les religions. Pourtant, les aspects religieux n''apparaissent jamais seuls en matière de géopolitique. Ils se mêlent aux éléments politiques, sociaux, économiques et surtout territoriaux.\r\nLe présent ouvrage se propose de lire les grandes dynamiques mondiales et de voir comment les religions les portent ou sont portées par elles. Davantage qu''un catalogue des géopolitiques de chacune d''entre elles, on verra quels sont les liens entre religions et identités, religions et territoires, religions et économie, religions et mondialisation. Davantage qu''un inventaire des conflits présentant une dimension religieuse, on s''attachera à montrer quelles logiques sont à l''oeuvre. On verra ainsi comment, en tentant de mettre en place des lois éternelles dans le monde d''aujourd''hui, les croyants contribuent à créer le monde de demain.', ' Presses universitaires de Fra', 1, ''),
(24, 8, 'Géopolitique des religions', 'Didier Giorgini', 'La géopolitique, au XXe siècle, a tenté de mettre en place de grands systèmes d''interprétation du monde, liés aux facteurs politiques, économiques et sociaux. Le XXIe siècle s''ouvre sur un coup de théâtre : il ne semble plus possible de comprendre le monde sans prendre en compte des aspects beaucoup moins quantifiables. Il s''agit du retour d''anciens « moteurs du monde » qu''on croyait grippés ou relégués en pièces de musée : les civilisations et les religions. Pourtant, les aspects religieux n''apparaissent jamais seuls en matière de géopolitique. Ils se mêlent aux éléments politiques, sociaux, économiques et surtout territoriaux.\r\nLe présent ouvrage se propose de lire les grandes dynamiques mondiales et de voir comment les religions les portent ou sont portées par elles. Davantage qu''un catalogue des géopolitiques de chacune d''entre elles, on verra quels sont les liens entre religions et identités, religions et territoires, religions et économie, religions et mondialisation. Davantage qu''un inventaire des conflits présentant une dimension religieuse, on s''attachera à montrer quelles logiques sont à l''oeuvre. On verra ainsi comment, en tentant de mettre en place des lois éternelles dans le monde d''aujourd''hui, les croyants contribuent à créer le monde de demain.', 'Presses universitaires de Fran', 1, ''),
(25, 8, 'Géopolitique des religions', 'Didier Giorgini', 'La géopolitique, au XXe siècle, a tenté de mettre en place de grands systèmes d''interprétation du monde, liés aux facteurs politiques, économiques et sociaux. Le XXIe siècle s''ouvre sur un coup de théâtre : il ne semble plus possible de comprendre le monde sans prendre en compte des aspects beaucoup moins quantifiables. Il s''agit du retour d''anciens « moteurs du monde » qu''on croyait grippés ou relégués en pièces de musée : les civilisations et les religions. Pourtant, les aspects religieux n''apparaissent jamais seuls en matière de géopolitique. Ils se mêlent aux éléments politiques, sociaux, économiques et surtout territoriaux.\r\nLe présent ouvrage se propose de lire les grandes dynamiques mondiales et de voir comment les religions les portent ou sont portées par elles. Davantage qu''un catalogue des géopolitiques de chacune d''entre elles, on verra quels sont les liens entre religions et identités, religions et territoires, religions et économie, religions et mondialisation. Davantage qu''un inventaire des conflits présentant une dimension religieuse, on s''attachera à montrer quelles logiques sont à l''oeuvre. On verra ainsi comment, en tentant de mettre en place des lois éternelles dans le monde d''aujourd''hui, les croyants contribuent à créer le monde de demain.', ' Presses universitaires de Fra', 1, ''),
(26, 8, 'Géopolitique des religions', 'Didier Giorgini', 'La géopolitique, au XXe siècle, a tenté de mettre en place de grands systèmes d''interprétation du monde, liés aux facteurs politiques, économiques et sociaux. Le XXIe siècle s''ouvre sur un coup de théâtre : il ne semble plus possible de comprendre le monde sans prendre en compte des aspects beaucoup moins quantifiables. Il s''agit du retour d''anciens « moteurs du monde » qu''on croyait grippés ou relégués en pièces de musée : les civilisations et les religions. Pourtant, les aspects religieux n''apparaissent jamais seuls en matière de géopolitique. Ils se mêlent aux éléments politiques, sociaux, économiques et surtout territoriaux.\r\nLe présent ouvrage se propose de lire les grandes dynamiques mondiales et de voir comment les religions les portent ou sont portées par elles. Davantage qu''un catalogue des géopolitiques de chacune d''entre elles, on verra quels sont les liens entre religions et identités, religions et territoires, religions et économie, religions et mondialisation. Davantage qu''un inventaire des conflits présentant une dimension religieuse, on s''attachera à montrer quelles logiques sont à l''oeuvre. On verra ainsi comment, en tentant de mettre en place des lois éternelles dans le monde d''aujourd''hui, les croyants contribuent à créer le monde de demain.', ' Presses universitaires de France ', 1, ''),
(27, 9, 'L''île des secrets', 'Nora Roberts', 'Lorsque ses amis lui présentent Nicholas Gregoras, le célèbre milliardaire grec, Morgan retient une exclamation de surprise. Ces yeux sombres, cette silhouette... Ils appartiennent à l''inconnu qui, la nuit précédente, alors qu''elle venait de prendre un bain de minuit, s''est jeté sur elle et l''a gardée quelques instants prisonnière tandis que des bruits de voix mystérieux s''élevaient sur la plage. Menace ou protection ? Impossible à dire. Et voilà que cet inconnu se tient de nouveau face à elle, ténébreux, dangereusement séduisant.\r\nTroublée par le souvenir de leur étrange étreinte, paralysée par ce regard qui semble lui intimer le silence sur ce qui s''est passé, Morgan pressent que la présence de Nicholas Gregoras sur l''île de Lesbos ne doit rien au hasard. Et qu''en le laissant approcher d''elle, c''est le danger qu''elle fait entrer dans sa vie.', 'Mosaic ›', 0, ''),
(28, 9, 'Scandaleux héritage', 'Jackie Collins', 'Red Diamond est un milliardaire brutal et détesté.\r\nDiahann, belle femme noire, ex-chanteuse, est sa gouvernante - un rôle que sa fille, Liberty, n''approuve pas. Le jour où Red convoque à New York ses trois fils, Max, Chris et Jett, ces derniers sont loin de se douter que cette réunion de famille va bouleverser leur monde.\r\nJeune héritière new-yorkaise, Amy Scott-Simon est fiancée à Max. À sa soirée d''enterrement de vie de jeune fille, elle rencontre Jett. Jett n''a pas la moindre idée de qui est Amy. Pas plus qu''elle ne comprend qui il est. Leur aventure d''une nuit va entraîner les pires complications.\r\nUne histoire d''amour, de pouvoir et d''argent au suspense haletant, comme seule Jackie Collins en a le secret !', 'Charleston ›', 0, ''),
(30, 10, 'Je me libère de mes phobies', 'Jérôme Palazzolo', 'De nombreuses personnes sont atteintes par une phobie qui empoisonne leur vie et celle de leur entourage. Dans la plupart des cas, elles ne comprennent pas ce qui leur arrive : on se retrouve un beau jour aux prises avec cette pathologie venue de nulle part, qui ne fait que croître sans qu''on puisse expliquer pourquoi.\r\nUn ouvrage qui explore toutes les pistes permettant d''appréhender une phobie, et expose tous les moyens thérapeutiques adaptés pour la combattre.', ' Presses Universitaires de France - PUF ›', 0, ''),
(32, 10, 'Le vieillissement psychique', 'Benoît Verdon', 'Les femmes et les hommes ne sont pas inertes face à leur vieillissement, lequel s''impose pourtant à eux de façon contraignante. Vieillir engage en effet un travail psychique intense qui mobilise les fragilités de chacun, au risque de la souffrance, et parfois de la pathologie, mais aussi ses ressources et ses potentialités de changement. En explorant les principaux aspects d''une aventure marquée par l''intranquillité et aussi communément partagée qu''éminemment subjective, cet ouvrage montre comment la confrontation à la finitude, à la moindre capacité, ne fait pas en soi de la vieillesse un temps de soumission, mais un temps de compromis, de construction et d''invention pour vivre encore et, parfois, se rencontrer enfin.', ' Presses Universitaires de France - PUF ›', 0, ''),
(34, 11, 'Bibliolycée Pro Boule de suif - Mademoiselle Fifi', 'Guy de Maupassant', 'Pour obtenir les meilleures nouvelles réalistes, il faut :\r\n- Un contexte historique réel : la guerre franco-prussienne de 1870, ou plutôt la débâcle française et l''occupation prussienne.\r\n- Des personnages typiques, notamment : des bourgeois, des représentants de la vieille noblesse, un républicain, des religieuses, des prostituées, sans oublier les officiers prussiens. Ajoutez des préjugés, et des situations inconfortables et déstabilisantes, dans des espaces clos. Tout y est : laissez agir le talent, l''écriture précise, lucide et nerveuse de Maupassant.\r\nVous allez voir apparaître ce que le confort et la paix dissimulent si bien : la vraie nature des uns et des autres, leur valeur humaine, ce qu''ils sont prêts à défendre et jusqu''où.\r\nL''ouvrage propose :\r\n- Le texte intégral annoté des deux nouvelles.\r\n- Une présentation originale et efficace de Maupassant et de son époque.\r\n- 9 questionnaires d''analyse des oeuvres conçus par une enseignante en lycée professionnel, en adéquation avec les programmes de français du baccalauréat professionnel.\r\n- Un dossier entièrement exploitable par l''élève.', 'Hachette Éducation ›', 0, ''),
(35, 11, 'Le Nouvel A portée de maths CM2 ', 'Jean-Claude Lucas, Janine Leclec''h - Lucas, Robert Meunier, Laurence Meunier, Marie-Pierre Trossevin', 'Une organisation en 4 domaines mathématiques accompagnée d''une progression annuelle pour aider à la programmation des activités (Nombres et calcul, Grandeurs et mesures, Espace et géométrie, Calcul mental).\r\nUne partie "Problèmes" intégrant des leçons de méthodologie et des problèms transversaux pour travailler des notions d''un même domaine ou de plusieurs domaines.\r\nL''intégration d''activités numériques.\r\nL''ajout d''exercices et de problèmes interdisciplinaires.', 'Hachette Éducation ›', 1, ''),
(37, 13, 'Guide Evasion Andalousie', 'Denis Montagnon, Stéphanie Chemla, Rozen Le Roux', 'Vous aimez construire votre propre circuit, sélectionner soigneusement vos étapes, dénicher un hôtel de charme, partir à la découverte de lieux authentiques ? Le guide Évasion est fait pour vous.\r\n\r\nVous y trouverez :\r\n\r\nTous les sites et monuments incontournables.\r\nDes cartes et des suggestions de circuits avec des infos précises pour identifier les meilleures étapes, les distances, les temps de transport…\r\nDes « balades secrètes » et des « coups de cœur de l’auteur » pour sortir des sentiers battus.\r\nDes adresses d’hôtels, de restaurants, de pauses… testées par nos auteurs.\r\nDes infos pratiques et des conseils pour composer son voyage sur mesure.\r\n\r\nLes guides Évasion sont réalisés par une équipe d’auteurs et d’experts locaux qui arpentent le pays pour en donner le meilleur !\r\n', 'Hachette Tourisme ›', 1, ''),
(38, 4, 'rrrrrr', 'joe', 'livre lesson', 'paul mazoto', 1, '');

-- --------------------------------------------------------

--
-- Structure de la table `livre_motcle`
--

CREATE TABLE IF NOT EXISTS `livre_motcle` (
  `IDLIVREMOTCLE` int(11) NOT NULL AUTO_INCREMENT,
  `BOOKID` int(11) NOT NULL,
  `IDMOCLEES` int(11) NOT NULL,
  PRIMARY KEY (`IDLIVREMOTCLE`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Contenu de la table `livre_motcle`
--

INSERT INTO `livre_motcle` (`IDLIVREMOTCLE`, `BOOKID`, `IDMOCLEES`) VALUES
(1, 6, 12),
(2, 6, 10),
(3, 11, 13),
(4, 11, 2),
(5, 10, 3),
(6, 10, 13),
(7, 10, 2),
(8, 6, 2),
(9, 6, 5),
(10, 6, 7),
(11, 38, 2),
(12, 38, 7),
(13, 38, 11);

-- --------------------------------------------------------

--
-- Structure de la table `mot_cles`
--

CREATE TABLE IF NOT EXISTS `mot_cles` (
  `IDMOCLEES` int(11) NOT NULL AUTO_INCREMENT,
  `INTITULEMOTCLE` varchar(90) DEFAULT NULL,
  PRIMARY KEY (`IDMOCLEES`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Contenu de la table `mot_cles`
--

INSERT INTO `mot_cles` (`IDMOCLEES`, `INTITULEMOTCLE`) VALUES
(1, 'internet'),
(2, 'etudes'),
(3, 'systeme d''information'),
(4, 'musique'),
(5, 'Aventures '),
(6, 'passions'),
(7, 'Ados'),
(8, 'vacances'),
(9, 'Nature'),
(10, 'Jeux'),
(11, 'guides pratiques'),
(12, 'jeunesses'),
(13, 'Enseignement superieur');

-- --------------------------------------------------------

--
-- Structure de la table `types`
--

CREATE TABLE IF NOT EXISTS `types` (
  `IDTYPES` int(11) NOT NULL AUTO_INCREMENT,
  `INTITULETYPES` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`IDTYPES`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `types`
--

INSERT INTO `types` (`IDTYPES`, `INTITULETYPES`) VALUES
(1, 'bibliothécaire'),
(2, 'client');

-- --------------------------------------------------------

--
-- Structure de la table `utilisateurs`
--

CREATE TABLE IF NOT EXISTS `utilisateurs` (
  `UTILISATEURSID` int(11) NOT NULL AUTO_INCREMENT,
  `IDTYPES` int(11) NOT NULL,
  `NOM` varchar(30) NOT NULL,
  `PRENOM` varchar(30) NOT NULL,
  `DATENAISSANCE` date NOT NULL,
  `MAIL` varchar(30) DEFAULT NULL,
  `LOGIN` varchar(25) NOT NULL,
  `MDP` varchar(255) NOT NULL,
  `NOTIFGCM` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`UTILISATEURSID`),
  KEY `AVOIR_TYPES_FK` (`IDTYPES`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Contenu de la table `utilisateurs`
--

INSERT INTO `utilisateurs` (`UTILISATEURSID`, `IDTYPES`, `NOM`, `PRENOM`, `DATENAISSANCE`, `MAIL`, `LOGIN`, `MDP`, `NOTIFGCM`) VALUES
(1, 1, 'rakoto', 'zafy', '1990-08-01', 'zafy@gmail.com', 'zafy', 'zafykoto', NULL),
(2, 2, 'rafalisoa', 'zanety', '2000-03-12', 'zanety@hotmail.com', 'zanety', 'test', NULL),
(3, 2, 'Brain', 'Jack', '1995-11-10', 'jack@mail.com', 'jack', 'jack', NULL),
(4, 2, 'goldman', 'jean', '1996-03-01', 'goldman@mail.com', 'goldman', 'goldman', NULL),
(5, 2, 'Crown', 'Hestia', '1996-07-02', 'hestia@mail.com', 'hestia', 'hestia', NULL),
(6, 2, 'jaggerjack', 'grimmjow', '1992-03-11', 'grim@mail.com', 'grimmjow', 'test', NULL),
(7, 2, 'schiffer', 'ulquiorra', '2016-03-07', 'ulqui@mail.com', 'ulquiora', 'test', NULL),
(8, 2, 'eponge', 'bob', '2000-03-26', 'bob@mail.com', 'bob', 'test', NULL);

-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `view_livres_categorie`
--
CREATE TABLE IF NOT EXISTS `view_livres_categorie` (
`BOOKID` int(11)
,`IDCATEGORIE` int(11)
,`TITRE` varchar(80)
,`AUTEUR` varchar(100)
,`RESUME` text
,`EDITEUR` varchar(80)
,`ETAT` tinyint(1)
,`INTITULECATEGORIE` varchar(30)
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `view_livres_empruntes`
--
CREATE TABLE IF NOT EXISTS `view_livres_empruntes` (
`UTILISATEURSID` int(11)
,`IDTYPES` int(11)
,`NOM` varchar(30)
,`PRENOM` varchar(30)
,`DATENAISSANCE` date
,`MAIL` varchar(30)
,`LOGIN` varchar(25)
,`MDP` varchar(255)
,`NOTIFGCM` varchar(250)
,`BOOKID` int(11)
,`IDCATEGORIE` int(11)
,`TITRE` varchar(80)
,`AUTEUR` varchar(100)
,`RESUME` text
,`EDITEUR` varchar(80)
,`ETAT` tinyint(1)
,`INTITULECATEGORIE` varchar(30)
,`DATEEMPRUNTS` date
,`DATERESTITUTION` date
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `view_utilisateur`
--
CREATE TABLE IF NOT EXISTS `view_utilisateur` (
`UTILISATEURSID` int(11)
,`IDTYPES` int(11)
,`NOM` varchar(30)
,`PRENOM` varchar(30)
,`DATENAISSANCE` date
,`MAIL` varchar(30)
,`LOGIN` varchar(25)
,`MDP` varchar(255)
,`NOTIFGCM` varchar(250)
,`INTITULETYPES` varchar(50)
);
-- --------------------------------------------------------

--
-- Structure de la vue `view_livres_categorie`
--
DROP TABLE IF EXISTS `view_livres_categorie`;

CREATE ALGORITHM=TEMPTABLE DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_livres_categorie` AS select `l`.`BOOKID` AS `BOOKID`,`l`.`IDCATEGORIE` AS `IDCATEGORIE`,`l`.`TITRE` AS `TITRE`,`l`.`AUTEUR` AS `AUTEUR`,`l`.`RESUME` AS `RESUME`,`l`.`EDITEUR` AS `EDITEUR`,`l`.`ETAT` AS `ETAT`,`c`.`INTITULECATEGORIE` AS `INTITULECATEGORIE` from (`livres` `l` join `categorie` `c`) where (`l`.`IDCATEGORIE` = `c`.`IDCATEGORIE`);

-- --------------------------------------------------------

--
-- Structure de la vue `view_livres_empruntes`
--
DROP TABLE IF EXISTS `view_livres_empruntes`;

CREATE ALGORITHM=TEMPTABLE DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_livres_empruntes` AS select `u`.`UTILISATEURSID` AS `UTILISATEURSID`,`u`.`IDTYPES` AS `IDTYPES`,`u`.`NOM` AS `NOM`,`u`.`PRENOM` AS `PRENOM`,`u`.`DATENAISSANCE` AS `DATENAISSANCE`,`u`.`MAIL` AS `MAIL`,`u`.`LOGIN` AS `LOGIN`,`u`.`MDP` AS `MDP`,`u`.`NOTIFGCM` AS `NOTIFGCM`,`v`.`BOOKID` AS `BOOKID`,`v`.`IDCATEGORIE` AS `IDCATEGORIE`,`v`.`TITRE` AS `TITRE`,`v`.`AUTEUR` AS `AUTEUR`,`v`.`RESUME` AS `RESUME`,`v`.`EDITEUR` AS `EDITEUR`,`v`.`ETAT` AS `ETAT`,`v`.`INTITULECATEGORIE` AS `INTITULECATEGORIE`,`e`.`DATEEMPRUNTS` AS `DATEEMPRUNTS`,`e`.`DATERESTITUTION` AS `DATERESTITUTION` from ((`emprunts` `e` join `utilisateurs` `u`) join `view_livres_categorie` `v`) where ((`e`.`UTILISATEURSID` = `u`.`UTILISATEURSID`) and (`e`.`BOOKID` = `v`.`BOOKID`));

-- --------------------------------------------------------

--
-- Structure de la vue `view_utilisateur`
--
DROP TABLE IF EXISTS `view_utilisateur`;

CREATE ALGORITHM=TEMPTABLE DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_utilisateur` AS select `u`.`UTILISATEURSID` AS `UTILISATEURSID`,`u`.`IDTYPES` AS `IDTYPES`,`u`.`NOM` AS `NOM`,`u`.`PRENOM` AS `PRENOM`,`u`.`DATENAISSANCE` AS `DATENAISSANCE`,`u`.`MAIL` AS `MAIL`,`u`.`LOGIN` AS `LOGIN`,`u`.`MDP` AS `MDP`,`u`.`NOTIFGCM` AS `NOTIFGCM`,`t`.`INTITULETYPES` AS `INTITULETYPES` from (`utilisateurs` `u` join `types` `t`) where (`u`.`IDTYPES` = `t`.`IDTYPES`);

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `emprunts`
--
ALTER TABLE `emprunts`
  ADD CONSTRAINT `emprunts_ibfk_1` FOREIGN KEY (`UTILISATEURSID`) REFERENCES `utilisateurs` (`UTILISATEURSID`),
  ADD CONSTRAINT `emprunts_ibfk_2` FOREIGN KEY (`BOOKID`) REFERENCES `livres` (`BOOKID`);

--
-- Contraintes pour la table `livres`
--
ALTER TABLE `livres`
  ADD CONSTRAINT `livres_ibfk_1` FOREIGN KEY (`IDCATEGORIE`) REFERENCES `categorie` (`IDCATEGORIE`);

--
-- Contraintes pour la table `utilisateurs`
--
ALTER TABLE `utilisateurs`
  ADD CONSTRAINT `utilisateurs_ibfk_1` FOREIGN KEY (`IDTYPES`) REFERENCES `types` (`IDTYPES`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
