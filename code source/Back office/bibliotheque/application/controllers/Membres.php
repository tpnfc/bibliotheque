<?php
/**
 * Created by PhpStorm.
 * User: Fanantenana
 * Date: 11/04/16
 * Time: 20:54
 */

class Membres extends CI_Controller{

    function __construct()
    {
        parent::__construct();
        $this->load->library('pagination');
        $this->load->model('Livre_model','livre_model');
        $this->load->model('Utilisateur_model','utilisateur_model');

    }

    function login()
    {
        if($this->input->post())
        {
            if($this->session->set_userdata('user_detail')){
                redirect( base_url() );
            }
            $mail = $this->input->post('login');
            $mdp = $this->input->post('mdp');

            $user = $this->utilisateur_model->get_login($mail,$mdp); //verify login

            $data=array();


             if($user) {
                $this->session->set_userdata('user_detail',$user);//activer sessionn
                 $data['flash_msg']=true;
                redirect( base_url('welcome/index') );
            }

            else{

                $data['flash_msg']=false;
                $email = $this->utilisateur_model->count_mail($mail);
                if( $email){
                    //$data['title'] = $this->titre->set('Mots de passe incorrecte!');
                    $data['error'] = "Mots de passe incorrecte!";
                }
                else{
                    // $data['title'] = $this->titre->set('Not membre!');
                    $data['error'] = "Pas encore insrit!";
                }
                $data['login'] = $mail;
                $data['mpd']  = $mdp;
                $this->load->view('login', $data);
            }
        }
        else{
            $this->load->view('login');

        }
    }

    public function users(){
        $data=array();

        $data['users']=$this->utilisateur_model->get_all_users();
        $this->load->view('user_list', $data);
    }


    function logout(){
        $this->session->unset_userdata('user_detail');
        $this->session->sess_destroy();
        redirect( base_url() );
    }


} 