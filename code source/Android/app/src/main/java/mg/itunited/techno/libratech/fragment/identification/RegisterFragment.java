package mg.itunited.techno.libratech.fragment.identification;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.HashMap;
import java.util.Map;

import mg.itunited.techno.libratech.R;
import mg.itunited.techno.libratech.activity.IdentificationActivity;
import mg.itunited.techno.libratech.utils.ConnectionDetector;
import mg.itunited.techno.libratech.utils.CustomRequest;
import mg.itunited.techno.libratech.utils.VolleyApplication;

/**
 * A simple {@link Fragment} subclass.
 */
public class RegisterFragment extends Fragment {

    private Button btnCancel;
    private Button btnConfirm;
    private Map<String,String> params = new HashMap<String,String>();
    private ConnectionDetector connectionDetector;
    private String NOM;
    private String PRENOM;
    private String EMAIL;
    private String LOGIN;
    private String MDP;
    private String CONFMDP;
    private EditText editNom;
    private EditText editPrenom;
    private EditText editEmail;
    private EditText editLogin;
    private EditText editMdp;
    private EditText editConfMdp;
    private ProgressDialog pDialog;
    private String jsonToFetch = "http://ocean-cash.com/bibliotheque/api/webservice/inscription";

    public RegisterFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        connectionDetector = new ConnectionDetector(getContext());

        View rootView = inflater.inflate(R.layout.fragment_register, container, false);

        btnCancel = (Button) rootView.findViewById(R.id.button_cancel);
        btnConfirm = (Button) rootView.findViewById(R.id.button_conf_inscription);

        editNom = (EditText) rootView.findViewById(R.id.edtxt_nom);
        editPrenom = (EditText) rootView.findViewById(R.id.edtxt_prenom);
        editEmail = (EditText) rootView.findViewById(R.id.edtxt_email);
        editLogin = (EditText) rootView.findViewById(R.id.edtxt_login);
        editMdp = (EditText) rootView.findViewById(R.id.edtxt_password);
        editConfMdp = (EditText) rootView.findViewById(R.id.edtxt_conf_password);

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ((IdentificationActivity)getActivity()).displayView(0);
            }
        });


        btnConfirm.setOnClickListener(new InscriptionListener());

        return rootView;
    }


    private class InscriptionListener implements View.OnClickListener{
        @Override
        public void onClick(View v) {

            pDialog = new ProgressDialog(getContext());
            pDialog.setMessage("Loading...");
            pDialog.show();

            NOM = editNom.getText().toString();
            PRENOM = editPrenom.getText().toString();
            EMAIL = editEmail.getText().toString();
            LOGIN = editLogin.getText().toString();
            MDP = editMdp.getText().toString();
            CONFMDP = editConfMdp.getText().toString();

            if(PasswordValidator(MDP,CONFMDP)){

                params.put("login",LOGIN);
                params.put("nom",NOM);
                params.put("prenom",PRENOM);
                params.put("email",EMAIL);
                params.put("mdp",MDP);
                fetch();
            }else {
                pDialog.dismiss();
                Toast.makeText(getContext(), "Mot de passe non identiques", Toast.LENGTH_SHORT).show();
                editConfMdp.setText("");
            }

        }
    }


    public void fetch(){


        StringRequest request =new StringRequest(Request.Method.POST,jsonToFetch,new MyJsonListener(),new MyJsonErrorListener()){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return params;
            }
        };
        VolleyApplication.getInstance().addToRequestQueue(request);
    }




    private class MyJsonListener implements Response.Listener<String> {

        @Override
        public void onResponse(String response) {
                pDialog.dismiss();
                ((IdentificationActivity)getActivity()).displayView(0);
                Toast.makeText(getContext(), " Inscription effectué \n Veuillez vous connecter", Toast.LENGTH_LONG).show();

        }
    }

    private class MyJsonErrorListener implements Response.ErrorListener{
        @Override
        public void onErrorResponse(VolleyError volleyError) {
            if(!connectionDetector.isConnectingToInternet()){
                pDialog.dismiss();
                Toast.makeText(getContext(), " Aucune connexion à Internet", Toast.LENGTH_LONG).show();
            }

        }
    }

    public boolean PasswordValidator(String pass,String confpass) {

        if(pass.equals(confpass)){
            return true;
        }else {
            return false;
        }
    }

}
