<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Ajout livres</title>
    <!-- BOOTSTRAP STYLES-->
    <?php
    echo load_css('bootstrap');
    echo load_css('custom');

    echo load_plugin_css('dataTables','.bootstrap');
    ?>

    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
</head>
<body>


<div id="wrapper">

    <!-- /. NAV TOP  -->

    <?php include(__DIR__.'/inc/nav-top.php') ?>

    <!-- /. NAV SIDE  -->
    <?php include(__DIR__.'/inc/nav-side.php') ?>
    <div id="page-wrapper" >
        <div id="page-inner">

                    <h2>Ajout livres </h2>


            <?php
            if(isset($flash_message)){
                if($flash_message == TRUE)
                {
                    echo '<div data-alert  id="log" class="alert alert-success">';

                    echo '<strong> Livres ajouté avec succès</strong>';
                    echo '<a href="#" class="close">&times;</a>';
                    echo '</div>';
                }
                else{

                    echo '<div data-alert class="alert alert-danger">';
                    echo '<strong>Erreur!</strong>';
                    echo '<a href="#" class="close">&times;</a>';
                    echo '</div>';
                }
            }

            ?>






                    <form action="<?php echo(base_url('livres/add')) ?>" method="post">


                        <div class="row">

                            <div class="col-md-8">

                                <div class="input-group">
                                    <span class="input-group-addon">Titre</span>
                                    <input type="text" name="titre" class="form-control" placeholder="Titre du livre" />
                                </div>

                                <br/>
                                <div class="input-group">
                                    <span class="input-group-addon">Auteur</span>
                                    <input type="text" class="form-control" placeholder="Auteur du livre" name="auteur" />
                                </div>
                                <br/>
                                <div class="input-group">
                                    <span class="input-group-addon">Editeur</span>
                                    <input type="text" class="form-control" placeholder="Editeur" name="editeur"/>
                                </div>
                                <br/>
                                <div class="input-group">
                                    <span class="input-group-addon">Resume</span>
                                    <textarea placeholder="resume du livre" name="resume" style="width: 500px"></textarea>
                                </div>
                                <br/>
                                <div class="input-group">
                                    <h4>Categorie</h4>
                                    <select id="ville" name="categorie"

                                        <option value="">-- Selectionnez categorie --</option>


                                        <?php
                                        foreach ($categorie as $row){

                                            echo '<option value="' .$row->IDCATEGORIE. '"   >'  . $row->INTITULECATEGORIE.   '</option>';

                                        }
                                        ?>


                                    </select>
                                </div>


                                <br/>
                                <div class="input-group">

                                    <h4>Selectionnez les mot-clees correspondant</h4>
                                    <?php foreach($mot_cle as $row){ ?>
                                    <input type="checkbox"  name="motcle[]" value="<?php echo $row->IDMOCLEES ?>"><?php echo $row->INTITULEMOTCLE ?><br>

                                    <?php } ?>
                                </div>

                            </div>

                        </div>




                        <button type="submit" class="btn btn-primary">valider</button>
                    </form>

        </div>
    </div>

</body>

</html>