package mg.itunited.techno.libratech.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.w3c.dom.Text;

import mg.itunited.techno.libratech.R;
import mg.itunited.techno.libratech.model.BookItem;

public class SearchDetailActivity extends AppCompatActivity {

    private TextView txtTitle;
    private TextView txtAuteur;
    private TextView txtResume;
    private TextView txtEditeur;
    private TextView txtCategorie;
    private TextView txtEtat;
    private Button btnRetour;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_detail);

        Intent intent =getIntent();
        BookItem book = intent.getExtras().getParcelable("detail");

         txtTitle = (TextView) findViewById(R.id.detail_search_titre);
         txtAuteur = (TextView) findViewById(R.id.detail_search_auteur);
         txtEditeur = (TextView) findViewById(R.id.detail_search_editeur);
         txtResume = (TextView) findViewById(R.id.detail_search_resume);
         txtEtat = (TextView) findViewById(R.id.detail_search_dispo);
        txtCategorie = (TextView) findViewById(R.id.detail_search_catego);

        txtTitle.setText(book.getTITRE());
        txtAuteur.setText(book.getAUTEUR());
        txtEditeur.setText(book.getEDITEUR());
        txtResume.setText(book.getRESUME());
        txtCategorie.setText(book.getCATEGORIE());


        if(book.getETAT()==1){

            txtEtat.setText("Disponible");
        }else {
            txtEtat.setText("Indisponible");
        }

        btnRetour = (Button) findViewById(R.id.button_search_retour);
        btnRetour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }
}
