<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Connexion</title>

    <?php
    echo load_css('bootstrap');
    echo load_css('styles');

    ?>


</head>

<body>

<?php
if(isset($flash_msg)){
    if($flash_msg == TRUE)
    {
        echo '<div data-alert  id="log" class="alert alert-success">';

        echo '<strong> Reussi</strong>';
        echo '<a href="#" class="close">&times;</a>';
        echo '</div>';
    }
    else{

        echo '<div data-alert class="alert alert-danger">';
        echo '<strong>Erreur!'.  $error.'</strong>';
        echo '<a href="#" class="close">&times;</a>';
        echo '</div>';
    }
}

?>

<div class="row">
    <div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4">
        <div class="login-panel panel panel-default">
            <div class="panel-heading">Connexion</div>
            <div class="panel-body">
                <form method="post" action="<?php echo base_url('membres/login') ?>">
                    <fieldset>
                        <div class="form-group">
                            <input class="form-control" placeholder="Login" name="login" type="text" autofocus="">
                        </div>
                        <div class="form-group">
                            <input class="form-control" placeholder="Mot de passe" name="mdp" type="password" value="">
                        </div>
                        <div class="checkbox">
                        </div>
                        <button class="btn btn-primary">Se connecter</button>
                    </fieldset>
                </form>
            </div>
        </div>
    </div><!-- /.col-->
</div><!-- /.row -->



<?php

echo load_js("jquery-1.10.2");
echo load_js("bootstrap.min");
echo load_js("custom");


echo load_plugin_js('dataTables','.min');
echo load_plugin_js('dataTables','.bootstrap');
?>
</body>

</html>
