package mg.itunited.techno.libratech.activity;

import android.app.PendingIntent;
import android.content.Intent;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;

import mg.itunited.techno.libratech.R;
import mg.itunited.techno.libratech.model.BookItem;
import mg.itunited.techno.libratech.utils.AppNFC;
import mg.itunited.techno.libratech.utils.OperationBook;

public class NFCSimilaireActivity extends AppCompatActivity {

    /*NFC adapter, pour traiter les tag*/
    private NfcAdapter nfcAdapter = null;
    private PendingIntent pendingIntent = null;
    private AppNFC appNFC;
    private OperationBook operationBook;
    private Button btnRetour;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nfc_similaire);

        appNFC =new AppNFC();
        operationBook =new OperationBook();
        nfcAdapter = NfcAdapter.getDefaultAdapter(this);
        pendingIntent = PendingIntent.getActivity(this, 0, new Intent(this, getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
        btnRetour = (Button) findViewById(R.id.button_similaire_retour);
        btnRetour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        nfcAdapter.enableForegroundDispatch(this, pendingIntent, null, null);
    }


    @Override
    protected void onPause() {
        super.onPause();
        nfcAdapter.disableForegroundDispatch(this);
    }

    @Override
    protected void onNewIntent(final Intent intent) {
        //Détection d'un périphérique NFC (tag ou autre)
        String action = intent.getAction();
        if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(action) || NfcAdapter.ACTION_NDEF_DISCOVERED.equals(action) || NfcAdapter.ACTION_TECH_DISCOVERED.equals(action))
        {

            String bookid = appNFC.LireTag(intent);

            operationBook.SimilaireLivre(bookid, getApplicationContext(), new OperationBook.VolleyCallback() {
                @Override
                public void onSuccess(boolean state) {
                }

                @Override
                public void onDataFetch(boolean state, ArrayList<BookItem> bookItems) {
                    if(state){
                        Intent acte = new Intent(getApplicationContext(),SimilaireActivity.class);
                        acte.putExtra("similaire",bookItems);
                        startActivity(acte);
                    }
                }
            });

        }
    }

}
