package mg.itunited.techno.libratech.fragment.main;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import mg.itunited.techno.libratech.R;
import mg.itunited.techno.libratech.activity.MainActivity;
import mg.itunited.techno.libratech.activity.MyBookActivity;
import mg.itunited.techno.libratech.utils.ConnectionDetector;
import mg.itunited.techno.libratech.utils.VolleyApplication;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {

    private Button btnEmprunter;
    private Button btnRendre;
    private Button btnMesEmprunts;
    private Button btnRecherche;
    private TextView nbrLivres;
    private ConnectionDetector connectionDetector;
    private Fragment fragment;
    private View rootView;
    private ProgressDialog pDialog;
    private String jsonToFetch ="http://ocean-cash.com/bibliotheque/api/webservice/count_emprunts";
    private Context mycontext;



    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mycontext = getContext();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
        int iduser = prefs.getInt("iduser",0);
        connectionDetector = new ConnectionDetector(getContext());
         rootView = inflater.inflate(R.layout.fragment_home, container, false);


        nbrLivres = (TextView) rootView.findViewById(R.id.text_nbr_livre_emprunt);

        btnEmprunter = (Button) rootView.findViewById(R.id.button_emprunt);
        btnRendre    = (Button) rootView.findViewById(R.id.button_rendre);
        btnMesEmprunts = (Button) rootView.findViewById(R.id.button_MesEmprunts);
        btnRecherche = (Button) rootView.findViewById(R.id.button_Recherche);

        btnEmprunter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                fragment = new EmpruntFragment();
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit();
            }
        });

        btnRendre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                fragment = new RetourFragment();
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit();

            }
        });

        btnMesEmprunts = (Button) rootView.findViewById(R.id.button_MesEmprunts);
        btnMesEmprunts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(rootView.getContext(), MyBookActivity.class);
                startActivity(intent);
            }
        });

        btnRecherche = (Button) rootView.findViewById(R.id.button_Recherche);
        btnRecherche.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment = new MyCategoryFragment();
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit();
            }
        });

        jsonToFetch = jsonToFetch+"?UTILISATEURSID="+iduser;

        pDialog = new ProgressDialog(getContext());
        pDialog.setMessage("Loading...");
        pDialog.show();

        fetch();

        return rootView;
    }




    public void fetch(){
        StringRequest request =new StringRequest(Request.Method.GET,jsonToFetch,new MyJsonListener(),new MyJsonErrorListener());
        VolleyApplication.getInstance().addToRequestQueue(request);
    }




    private class MyJsonListener implements Response.Listener<String> {

        @Override
        public void onResponse(String response) {
            pDialog.dismiss();
            nbrLivres.setText(response);

        }
    }

    private class MyJsonErrorListener implements Response.ErrorListener{
        @Override
        public void onErrorResponse(VolleyError volleyError) {
            if(!connectionDetector.isConnectingToInternet()){

                Toast.makeText(mycontext, " Aucune connexion à Internet", Toast.LENGTH_LONG).show();
            }else{

                Toast.makeText(mycontext, " Erreur de connexion", Toast.LENGTH_LONG).show();
            }
            pDialog.dismiss();
        }
    }










}
