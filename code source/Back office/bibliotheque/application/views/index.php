<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Tableau de bord</title>
    <!-- BOOTSTRAP STYLES-->
    <?php
    echo load_css('bootstrap');
    echo load_css('custom');
        ?>

    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
</head>
<body>



<div id="wrapper">

    <!-- /. NAV TOP  -->

    <?php include(__DIR__.'/inc/nav-top.php') ?>

    <!-- /. NAV SIDE  -->
    <?php include(__DIR__.'/inc/nav-side.php') ?>

    <div id="page-wrapper" >
        <div id="page-inner">
            <div class="row">
                <div class="col-md-12">
                    <h2>Tableau de bord </h2>
                </div>
            </div>
            <!-- /. ROW  -->
            <hr />

            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">

                    <div class="count"><?php echo $nb_livres ?></div>

                    <h3>Livres</h3>
                </div>
            </div>
            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">

                    <div class="count"><?php echo $nb_livres_dispo ?></div>

                    <h3>Livres dispo</h3>
                </div>
            </div>
            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">

                    <div class="count"><?php echo $nb_livres_empruntes ?></div>

                    <h3>Livres empruntes</h3>
                </div>
            </div>

            <!-- /. ROW  -->
        </div>
        <!-- /. PAGE INNER  -->
    </div>
    <!-- /. PAGE WRAPPER  -->
</div>
<div class="footer">


    <div class="row">
        <div class="col-lg-12" >
            &copy;  2014 yourdomain.com | Design by: <a href="http://binarytheme.com" style="color:#fff;"  target="_blank">www.binarytheme.com</a>
        </div>
    </div>
</div>


<!-- /. WRAPPER  -->
<!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
<!-- JQUERY SCRIPTS -->
<script src="assets/js/jquery-1.10.2.js"></script>
<!-- BOOTSTRAP SCRIPTS -->
<script src="assets/js/bootstrap.min.js"></script>
<!-- CUSTOM SCRIPTS -->
<script src="assets/js/custom.js"></script>

<?php

    echo load_js("jquery-1.10.2");
    echo load_js("bootstrap.min");
    echo load_js("custom.min");
?>


</body>
</html>
