package mg.itunited.techno.libratech.fragment.main;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import mg.itunited.techno.libratech.R;
import mg.itunited.techno.libratech.activity.BookDetailActivity;
import mg.itunited.techno.libratech.activity.CategoActivity;
import mg.itunited.techno.libratech.adapter.CategoAdapteur;
import mg.itunited.techno.libratech.model.BookItem;
import mg.itunited.techno.libratech.model.CategorieItem;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyCategoryFragment extends Fragment {

    private ListView listCatego;
    private CategoAdapteur adapteur;
    private ArrayList<CategorieItem> catego;
    private Button btnRetour;
    private Fragment fragment;

    public MyCategoryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View rootView =inflater.inflate(R.layout.fragment_my_category, container, false);


        catego = new ArrayList<>();

        catego.add(new CategorieItem("1","Bande dessine",R.drawable.bd));
        catego.add(new CategorieItem("2","Cuisine",R.drawable.cuisine));
        catego.add(new CategorieItem("3","Histoire",R.drawable.histoire));
        catego.add(new CategorieItem("4","Informatique",R.drawable.informatique));
        catego.add(new CategorieItem("5","Humour",R.drawable.humour));
        catego.add(new CategorieItem("6","Jeunesse",R.drawable.jeunesse));
        catego.add(new CategorieItem("7","Litterature",R.drawable.litterature));
        catego.add(new CategorieItem("8","Religion",R.drawable.religion));
        catego.add(new CategorieItem("9","Romance",R.drawable.romance));
        catego.add(new CategorieItem("10","Sciences humaines",R.drawable.sience));
        catego.add(new CategorieItem("11","Scolaire",R.drawable.scolaire));
        catego.add(new CategorieItem("13","Tourisme",R.drawable.tourisme));
        catego.add(new CategorieItem("14","Sport et loisirs",R.drawable.sport));
        catego.add(new CategorieItem("15","Aventure",R.drawable.aventure));

        listCatego = (ListView) rootView.findViewById(R.id.lv_categorie);
        adapteur = new CategoAdapteur(rootView.getContext(),R.layout.list_catego_row,catego);
        listCatego.setAdapter(adapteur);

        listCatego.setOnItemClickListener(new CategoListener());

        btnRetour =(Button) rootView.findViewById(R.id.button_retour_catego);

        btnRetour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment = new HomeFragment();
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit();
            }
        });


        return rootView;
    }

    private class CategoListener implements AdapterView.OnItemClickListener{

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            CategorieItem item = (CategorieItem) parent.getItemAtPosition(position);
            Intent intent = new Intent(view.getContext(), CategoActivity.class);
            intent.putExtra("catego",item.getIDCatego());
            startActivity(intent);
        }
    }


}
