<?php
/**
 * Created by PhpStorm.
 * User: Fanantenana
 * Date: 10/03/16
 * Time: 15:38
 */

require APPPATH.'/libraries/REST_Controller.php';
class Webservice extends REST_Controller{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Livre_model');

        $this->load->model('Emprunts_model');
        $this->load->model('Utilisateur_model');

    }


    function livre_get()
    {
        $id = $this->get('IDCATEGORIE');
        if($id!=null){
            $libres=$this->Livre_model->get_list_by_idcategorie($id);
            $rep=$this->response($libres, 200);
            return $rep;
        }
        else{
            $libres=$this->Livre_model->get_list();
            $rep=$this->response($libres, 200);
            return $rep;
        }
    }

    function livre_similaire_get()
    {
        $id = $this->get('BOOKID');
        if($id!=null){
            $libres=$this->Livre_model->get_livre_similaire($id);
            $rep=$this->response($libres, 200);
            return $rep;
        }
        else{
            $this->set_response([
                'status' => FALSE,
                'message' => 'Aucun livre similaire trouvee'
            ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code

        }
    }

    function emprunts_get()
    {
        $id = $this->get('UTILISATEURSID');
        if($id!=null){
            $libres=$this->Livre_model->get_livres_empruntes_by_userid($id);
            $rep=$this->response($libres, 200);
            return $rep;
        }
        else{
            $this->set_response([
                'status' => FALSE,
                'message' => 'Vous n\'avez empruntes aucun livre'
            ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }
    }


    //effectuer un emprunt
    function emprunts_post(){
        $bookid=$this->post("BOOKID");
        $data = array(
            'UTILISATEURSID' => $this->post("UTILISATEURSID"),
            'BOOKID' => $this->post("BOOKID"),
            'DATEEMPRUNTS' => $this->post('DATEEMPRUNTS'),
            'DATERESTITUTION' => $this->post('DATERESTITUTION')
        );
        //file_get_contents('php://input', 'r');
        $rep=$this->Emprunts_model->insert($data);
        if($rep)
        {
            $this->Emprunts_model->emprunt_upd($bookid);

        }
        $this->set_response($rep, REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP response code
    }



    //nb livres empruntes par utilisateur
    function count_emprunts_get()
    {
        $id = $this->get('UTILISATEURSID');
        if($id!=null){
            $libres=$this->Livre_model->count_livres_by_user($id);
            $rep=$this->response($libres, 200);
            return $rep;
        }
        else{
            $this->set_response([
                'status' => FALSE,
                'message' => 'Vous n\'avez empruntes aucun livre'
            ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }
    }


    function retour_get(){

        $id = $this->get('UTILISATEURSID');
        $bookid=$this->get('BOOKID');
        if($id!=null && $bookid!=null){
            $libres=$this->Livre_model->retour_livre_upd($bookid);
            $this->Livre_model->retour_livre_del($id,$bookid);
            $rep=$this->response([
                'status' => TRUE,
                'message' => 'Livre rendu avec succees '
            ], 200);
            return $rep;
        }
        else{
            $this->set_response([
                'status' => FALSE,
                'message' => 'Erreur '
            ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }
    }

    function inscription_post(){

        $data = array(
            'PRENOM'=>$this->post("prenom"),
            'NOM'=>$this->post("nom"),
            'MAIL'=>$this->post("email"),
            'LOGIN'=>$this->post("login"),
            'MDP'=>$this->post("mdp"),
            'IDTYPES'=>2
        );
        $this->Utilisateur_model->add_utilisateurs($data);
        $this->set_response($data, REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP response code

    }

    function login_post()
    {
        $user=null;
        $login=$this->post('login');
        $pass=$this->post('pass');
        if(!$login && !$pass)
        {
            $this->response(NULL, 400);
        }
        if($login!=null && $pass!=null)
        {
            $user = $this->Utilisateur_model->get_login($login,$pass);
        }
        if($user)
        {
            $this->response($user, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(array('error' => 'User could not be found'), 404);
        }
    }
} 