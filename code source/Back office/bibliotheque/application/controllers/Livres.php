<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Livres extends CI_Controller {


    function __construct()
    {
        parent::__construct();
        $this->load->library('pagination');
        $this->load->model('Livre_model','livre_model');

    }





    public function index()
    {
        $data['nb_livres']=$this->livre_model->count_livres();
        $data['nb_livres_dispo']=$this->livre_model->ccount_livres_dispo();
        $data['nb_livres_empruntes']=$this->livre_model->ccount_livres_empruntes();
        $this->load->view('index',$data);
    }

    public function all(){
        $data=array();
        $data['livres']=$this->livre_model->get_list();
        $this->load->view('livres',$data);
    }

    public function dispo(){
        $data=array();
        $data['livres']=$this->livre_model->get_livres_dispo();
        $this->load->view('livres_dispo',$data);
    }

    public function emprunts(){
        $data=array();
        $data['livres']=$this->livre_model->get_livres_empruntees();
        $this->load->view('livres_empruntes',$data);
    }


    public function add()
    {

        $data=array();
        $data['mot_cle']=$this->livre_model->get_mot_cles();
        $data['categorie']=$this->livre_model->get_categorie();

        if ($this->input->post())
        {

            $this->load->helper(array('form', 'url'));

            $this->load->library('form_validation');

            $this->form_validation->set_rules('titre', 'titre', 'required');
            $this->form_validation->set_rules('auteur', 'auteur', 'required');
            $this->form_validation->set_rules('editeur', 'editeur', 'required');
            $this->form_validation->set_rules('resume', 'resume', 'required');
            $this->form_validation->set_rules('categorie', 'categorie', 'required');
            if ($this->form_validation->run() == FALSE)
            {

                $data['flash_message'] = false;
            }

            else{

                $motcle= $this->input->post('motcle');
                $titre= $this->input->post('titre');
                $auteur= $this->input->post('auteur');
                $editeur= $this->input->post('editeur');
                $resume= $this->input->post('resume');
                $categorie= $this->input->post('categorie');

                $le_livre=array(
                    'TITRE'=>$titre,
                    'AUTEUR'=>$auteur,
                    'EDITEUR'=>$editeur,
                    'RESUME'=>$resume,
                    'IDCATEGORIE'=>$categorie
                );

                $id=  $this->livre_model->add_livres($le_livre);

                    if($id!=null){

                        $this->livre_model->add_mot_cles($id,$motcle);

                    }
                $data['flash_message'] = TRUE;

            }


        }
        $this->load->view('add',$data);
    }


}
