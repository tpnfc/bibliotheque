package mg.itunited.techno.libratech.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import mg.itunited.techno.libratech.R;
import mg.itunited.techno.libratech.model.BookItem;

/**
 * Created by Andrianina on 16/04/2016.
 */
public class CategoBookAdapteur extends ArrayAdapter<BookItem> {
    private ArrayList<BookItem> BookList;
    private LayoutInflater layoutInflater;

    public CategoBookAdapteur(Context context, int resource, List<BookItem> objects) {
        super(context, resource, objects);
        this.layoutInflater = LayoutInflater.from(context);
        this.BookList = new ArrayList<BookItem>();
        this.BookList.addAll(objects);


    }


    private class ViewHolder{

        TextView titre;
        TextView auteur;
        TextView categorie;
        TextView disponible;

    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;

        if(convertView == null){

            convertView = layoutInflater.inflate(R.layout.list_recherche_row,null);
            viewHolder = new ViewHolder();
            viewHolder.titre = (TextView) convertView.findViewById(R.id.row_recherche_title);
            viewHolder.auteur = (TextView) convertView.findViewById(R.id.row_recherche_auteur);
            viewHolder.categorie = (TextView) convertView.findViewById(R.id.row_recherche_catego);
            viewHolder.disponible = (TextView) convertView.findViewById(R.id.row_recherche_etat);


            convertView.setTag(viewHolder);

        }else{

            viewHolder = (ViewHolder) convertView.getTag();
        }

        BookItem bookItem = BookList.get(position);
        viewHolder.titre.setText(bookItem.getTITRE());
        viewHolder.auteur.setText(bookItem.getAUTEUR());
        viewHolder.categorie.setText(bookItem.getCATEGORIE());
        if(bookItem.getETAT()==1){
            viewHolder.disponible.setText("Disponible");
        }else {
            viewHolder.disponible.setText("Indisponible");
        }



        return convertView;
    }


    public void swapBookRecords(ArrayList<BookItem> bookItems){

        clear();
        BookList.clear();
        BookList.addAll(bookItems);
        addAll(bookItems);
        notifyDataSetChanged();
    }


}
