package mg.itunited.techno.libratech.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import mg.itunited.techno.libratech.R;

public class SplashScreenActivity extends AppCompatActivity {

    private int ms=0;
    private int splashTime=2000;
    private boolean splashActive = true;
    private boolean paused = false;

    private String nom;
    private String prenom;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);


        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        nom = prefs.getString("nom","");
        prenom = prefs.getString("prenom","");


        Thread mythread = new Thread(){

            public void run(){
                try{
                    while (splashActive && ms < splashTime) {
                        if(!paused)
                            ms=ms+100;
                        sleep(100);
                    }

                }catch (Exception e){


                }finally {

                    if(nom.equals("")&&prenom.equals("")){
                        Intent intent = new Intent(SplashScreenActivity.this, IdentificationActivity.class);
                        startActivity(intent);
                    }else {

                        Intent intent = new Intent(SplashScreenActivity.this, MainActivity.class);
                        startActivity(intent);
                    }

                }

            }


        };

        mythread.start();


    }
}
