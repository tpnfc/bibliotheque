package mg.itunited.techno.libratech.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Andrianina on 13/04/2016.
 */
public class UserItem implements Parcelable {

    private int UTILISATEURSID;
    private String NOM;
    private String PRENOM;
    private String MAIL;
    private String LOGIN;
    private String MDP;




    public UserItem(){}

    public UserItem(Parcel in){

        this(in.readInt(),in.readString(),in.readString(),in.readString(),in.readString(),in.readString());
    }

    public UserItem(int UTILISATEURSID, String NOM, String PRENOM, String MAIL, String LOGIN, String MDP) {
        this.UTILISATEURSID = UTILISATEURSID;
        this.NOM = NOM;
        this.PRENOM = PRENOM;
        this.MAIL = MAIL;
        this.LOGIN = LOGIN;
        this.MDP = MDP;
    }


    public int getUTILISATEURSID() {
        return UTILISATEURSID;
    }

    public void setUTILISATEURSID(int UTILISATEURSID) {
        this.UTILISATEURSID = UTILISATEURSID;
    }

    public String getNOM() {
        return NOM;
    }

    public void setNOM(String NOM) {
        this.NOM = NOM;
    }

    public String getPRENOM() {
        return PRENOM;
    }

    public void setPRENOM(String PRENOM) {
        this.PRENOM = PRENOM;
    }

    public String getMAIL() {
        return MAIL;
    }

    public void setMAIL(String MAIL) {
        this.MAIL = MAIL;
    }

    public String getLOGIN() {
        return LOGIN;
    }

    public void setLOGIN(String LOGIN) {
        this.LOGIN = LOGIN;
    }

    public String getMDP() {
        return MDP;
    }

    public void setMDP(String MDP) {
        this.MDP = MDP;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

    }



    public static final Parcelable.Creator<UserItem> CREATOR = new Creator<UserItem>()
    {
        @Override
        public UserItem createFromParcel(Parcel source)
        {
            return new UserItem(source);
        }

        @Override
        public UserItem[] newArray(int size)
        {
            return new UserItem[size];
        }
    };

}
