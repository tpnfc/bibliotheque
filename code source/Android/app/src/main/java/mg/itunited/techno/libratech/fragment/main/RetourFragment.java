package mg.itunited.techno.libratech.fragment.main;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import mg.itunited.techno.libratech.R;
import mg.itunited.techno.libratech.activity.NFCRetourActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class RetourFragment extends Fragment {

    private Button btnRetour;
    private Button btnCancel;
    private Fragment fragment;
    private View rootView;


    public RetourFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        rootView = inflater.inflate(R.layout.fragment_retour, container, false);

        btnRetour = (Button) rootView.findViewById(R.id.buttonRetour);

        btnCancel = (Button) rootView.findViewById(R.id.button_cancel_retour);

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment = new HomeFragment();
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit();
            }
        });

        btnRetour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(rootView.getContext(), NFCRetourActivity.class);
                startActivity(intent);
            }
        });
        return rootView;
    }

}
