package mg.itunited.techno.libratech.model;

/**
 * Created by Andrianina on 15/04/2016.
 */
public class CategorieItem {

    private String IDCatego;
    private String title;
    private int icon;

    public CategorieItem() {

    }

    public CategorieItem(String IDCatego,String title, int icon) {

        this.IDCatego = IDCatego;
        this.title = title;
        this.icon = icon;

    }

    public String getIDCatego() {
        return IDCatego;
    }

    public void setIDCatego(String IDCatego) {
        this.IDCatego = IDCatego;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getIcon(){
        return icon;
    }

    public void setIcon(int icon){
        this.icon=icon;
    }
}
