<nav class="navbar-default navbar-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav" id="main-menu">


            <li class= "<?=($this->uri->segment(2)==='index')?'active-link':''?>">
                <a  href="<?php echo base_url('livres/index') ?>" ><i class="fa fa-desktop "></i>Tableau de bord </a>
            </li>
            <li class="<?=($this->uri->segment(2)==='users')?'active-link':''?>">
                <a href="<?php echo base_url('membres/users') ?>"><i class="fa fa-users "></i>Utilisateurs </a>
            </li>
            <li class="<?=($this->uri->segment(2)==='all')?'active-link':''?>">
                <a href="<?php echo base_url('livres/all') ?>"><i class="fa fa-table "></i>Tous les livres </a>
            </li>
            <li class="<?=($this->uri->segment(2)==='dispo')?'active-link':''?>">
                <a href="<?php echo base_url('livres/dispo') ?>"><i class="fa fa-book "></i>Livres disponibles </a>
            </li>
            <li class="<?=($this->uri->segment(2)==='emprunts')?'active-link':''?>">
                <a href="<?php echo base_url('livres/emprunts') ?>"><i class="fa fa-edit "></i>Livres empruntes </a>
            </li>

        </ul>
    </div>



</nav>