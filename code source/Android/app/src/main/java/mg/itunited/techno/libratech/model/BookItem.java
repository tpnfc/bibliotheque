package mg.itunited.techno.libratech.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Andrianina on 11/04/2016.
 */
public class BookItem implements Parcelable{

    private int BOOKID;
    private int IDCATEGORIE;
    private String TITRE;
    private String AUTEUR;
    private String RESUME;
    private String EDITEUR;
    private String CATEGORIE;
    private String DEBUT_EMPRUNT;
    private String RESTITUTION_EMPRUNT;
    private int ETAT;


    public BookItem(){

    }

   public BookItem(Parcel in){

       this(in.readInt(),
               in.readInt(),
               in.readString(),
               in.readString(),
               in.readString(),
               in.readString(),
               in.readString(),
               in.readString(),in.readString(), in.readInt());

   }

    public BookItem(int BOOKID, int IDCATEGORIE, String TITRE, String AUTEUR, String RESUME, String EDITEUR, String CATEGORIE, String DEBUT_EMPRUNT, String RESTITUTION_EMPRUNT,int ETAT) {
        this.BOOKID = BOOKID;
        this.IDCATEGORIE = IDCATEGORIE;
        this.TITRE = TITRE;
        this.AUTEUR = AUTEUR;
        this.RESUME = RESUME;
        this.EDITEUR = EDITEUR;
        this.CATEGORIE = CATEGORIE;
        this.DEBUT_EMPRUNT = DEBUT_EMPRUNT;
        this.RESTITUTION_EMPRUNT = RESTITUTION_EMPRUNT;
        this.ETAT = ETAT;
    }

    public int getBOOKID() {
        return BOOKID;
    }

    public void setBOOKID(int BOOKID) {
        this.BOOKID = BOOKID;
    }

    public int getIDCATEGORIE() {
        return IDCATEGORIE;
    }

    public void setIDCATEGORIE(int IDCATEGORIE) {
        this.IDCATEGORIE = IDCATEGORIE;
    }

    public String getTITRE() {
        return TITRE;
    }

    public void setTITRE(String TITRE) {
        this.TITRE = TITRE;
    }

    public String getAUTEUR() {
        return AUTEUR;
    }

    public void setAUTEUR(String AUTEUR) {
        this.AUTEUR = AUTEUR;
    }

    public String getRESUME() {
        return RESUME;
    }

    public void setRESUME(String RESUME) {
        this.RESUME = RESUME;
    }

    public String getEDITEUR() {
        return EDITEUR;
    }

    public void setEDITEUR(String EDITEUR) {
        this.EDITEUR = EDITEUR;
    }

    public String getCATEGORIE() {
        return CATEGORIE;
    }

    public void setCATEGORIE(String CATEGORIE) {
        this.CATEGORIE = CATEGORIE;
    }

    public String getDEBUT_EMPRUNT() {
        return DEBUT_EMPRUNT;
    }

    public void setDEBUT_EMPRUNT(String DEBUT_EMPRUNT) {
        this.DEBUT_EMPRUNT = DEBUT_EMPRUNT;
    }

    public String getRESTITUTION_EMPRUNT() {
        return RESTITUTION_EMPRUNT;
    }

    public void setRESTITUTION_EMPRUNT(String RESTITUTION_EMPRUNT) {
        this.RESTITUTION_EMPRUNT = RESTITUTION_EMPRUNT;
    }

    public int getETAT() {
        return ETAT;
    }

    public void setETAT(int ETAT) {
        this.ETAT = ETAT;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeInt(BOOKID);
        dest.writeInt(IDCATEGORIE);
        dest.writeString(TITRE);
        dest.writeString(AUTEUR);
        dest.writeString(RESUME);
        dest.writeString(EDITEUR);
        dest.writeString(CATEGORIE);
        dest.writeString(DEBUT_EMPRUNT);
        dest.writeString(RESTITUTION_EMPRUNT);
        dest.writeInt(ETAT);

    }



    public static final Parcelable.Creator<BookItem> CREATOR = new Creator<BookItem>()
    {
        @Override
        public BookItem createFromParcel(Parcel source)
        {
            return new BookItem(source);
        }

        @Override
        public BookItem[] newArray(int size)
        {
            return new BookItem[size];
        }
    };



}
