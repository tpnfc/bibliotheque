<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: RICARDO
 * Date: 05/11/14
 * Time: 09:29
 */

if ( ! function_exists('assets_url'))
{
    function assets_url($nom)
    {
        return base_url() . 'assets/' . $nom ;
    }
}
if ( ! function_exists('css_url'))
{
    function css_url($nom)
    {
        return base_url() . 'assets/css/' . $nom .'.css' ;
    }
}
if ( ! function_exists('js_url'))
{
    function js_url($nom)
    {
        return base_url() . 'assets/js/' . $nom .'.js' ;
    }
}
//image en generale
if ( ! function_exists('img_url'))
{
    function img_url($nom)
    {
        return base_url() . 'assets/img/' . $nom;
    }
}

//image upload
if ( ! function_exists('upload_img_url'))
{
    function upload_img_url($nom)
    {
        return base_url() . 'assets/upload/' . $nom;
    }
}
//image profile
if ( ! function_exists('img_profileBig_url'))
{
    function img_profileBig_url($nom)
    {
        return base_url() . 'assets/img/smallbig/' . $nom;
    }
}
if ( ! function_exists('img_profile_url'))
{
    function img_profile_url($nom)
    {
        return base_url() . 'assets/img/small/' . $nom;
    }
}
if ( ! function_exists('img_profile_url_50'))
{
    function img_profile_url_50($nom)
    {
        return base_url() . 'assets/img/small50/' . $nom;
    }
}
if ( ! function_exists('img_profile_url_100'))
{
    function img_profile_url_100($nom)
    {
        return base_url() . 'assets/img/small100/' . $nom;
    }
}

//image dans le publication
if ( ! function_exists('img_pubBig_url'))
{
    function img_pubBig_url($nom)
    {
        return base_url() . 'assets/img/pub-big/' . $nom;
    }
}
if ( ! function_exists('img_pub_url'))
{
    function img_pub_url($nom)
    {
        return base_url() . 'assets/img/pub-small/' . $nom;
    }
}
if( !function_exists('img_pubFine_url'))
{
	function img_pubFine_url($nom)
    {
        return base_url() . 'assets/img/pub-fine/' . $nom;
    }
}


//plugin url
if ( ! function_exists('load_plugin_css'))
{
    function load_plugin_css($nom,$min='')
    {
        return '<link rel="stylesheet" href="'.base_url() . 'assets/plugin/' . $nom . '/css/' . $nom . $min .'.css" type="text/css" >';
    }
}
if ( ! function_exists('load_plugin_js'))
{
    function load_plugin_js($nom,$min='')
    {
        return '<script src="'.base_url() . 'assets/plugin/' . $nom . '/js/' . $nom . $min .'.js'.'" type="text/javascript"></script>';
    }
}
//plugin url
if ( ! function_exists('load_css'))
{
    function load_css($nom)
    {
        return '<link rel="stylesheet" href="' . base_url() . 'assets/css/' . $nom .'.css" type="text/css" >';
    }
}
if ( ! function_exists('load_js'))
{
    function load_js($nom)
    {
        return '<script src="'.base_url() . 'assets/js/' . $nom . '.js'.'" type="text/javascript"></script>';
    }
}