<?php
/**
 * Created by PhpStorm.
 * User: webtana
 * Date: 27/11/15
 * Time: 14:07
 */

class Utilisateur_model extends CI_Model{
    protected $table ='utilisateurs';
    protected $view ='view_utilisateur';

    public function __construct(){
        parent::__construct();
    }

    public function add_utilisateurs($data)
    {
        return $this->db
            ->insert($this->table, $data);
    }


    public function get_login($mail,$mdp)
    {
        return $this->db->select('*')
            ->from($this->table)
            ->where('LOGIN',  $mail)
            ->where('MDP',  $mdp)
            ->get()
            ->result();
    }


    public function get_all_users()
    {
        return $this->db->select('*')
            ->from($this->view)
            ->get()
            ->result();
    }


    public function get_mdp($mail)
    {
        return $this->db->select('*')
            ->from($this->table)
            ->where('LOGIN',  $mail)
            ->get()
            ->result();
    }

    function count_mail($email)
    {
        return (int)$this->db->where('LOGIN', $email)
            ->count_all_results($this->table);
    }



}
