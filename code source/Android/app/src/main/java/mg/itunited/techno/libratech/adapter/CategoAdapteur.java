package mg.itunited.techno.libratech.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import mg.itunited.techno.libratech.R;
import mg.itunited.techno.libratech.model.CategorieItem;

/**
 * Created by Andrianina on 15/04/2016.
 */
public class CategoAdapteur extends ArrayAdapter<CategorieItem> {

    ArrayList<CategorieItem> CategoList;
    LayoutInflater layoutInflater;

    public CategoAdapteur(Context context, int resource, List<CategorieItem> objects) {
        super(context, resource, objects);
        this.layoutInflater =LayoutInflater.from(context);
        this.CategoList = new ArrayList<CategorieItem>();
        this.CategoList.addAll(objects);
    }

    private class ViewHolder{
        ImageView icon;
        TextView  title;
    }




    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;

        if(convertView == null){
            convertView = layoutInflater.inflate(R.layout.list_catego_row,null);
            viewHolder = new ViewHolder();
            viewHolder.icon = (ImageView) convertView.findViewById(R.id.row_catego_icon);
            viewHolder.title = (TextView) convertView.findViewById(R.id.row_catego_title);
            convertView.setTag(viewHolder);

        }else {

            viewHolder = (ViewHolder) convertView.getTag();

        }

        CategorieItem item = CategoList.get(position);
        viewHolder.icon.setImageResource(item.getIcon());
        viewHolder.title.setText(item.getTitle());

        return convertView;
    }
}
