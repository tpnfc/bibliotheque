package mg.itunited.techno.libratech.fragment.main;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import mg.itunited.techno.libratech.R;
import mg.itunited.techno.libratech.activity.NFCSimilaireActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class SimilaireFragment extends Fragment {

    private Button btnSimilaire;
    private Button btnCancel;
    private Fragment fragment;
    private View rootView;


    public SimilaireFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment

         rootView = inflater.inflate(R.layout.fragment_similaire, container, false);

        btnSimilaire = (Button) rootView.findViewById(R.id.button_similaire);

        btnCancel = (Button) rootView.findViewById(R.id.button_cancel_similaire);

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment = new HomeFragment();
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit();
            }
        });

        btnSimilaire.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(rootView.getContext(), NFCSimilaireActivity.class);
                startActivity(intent);
            }
        });


        return rootView;
    }

}
