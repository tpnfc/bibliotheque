<?php
/**
 * Created by PhpStorm.
 * User: Fanantenana
 * Date: 10/03/16
 * Time: 15:39
 */


class Livre_model extends CI_Model {

    protected $table = 'livres';
    protected $table_mot_cles = 'livre_motcle';

    protected $view_livres_categorie='view_livres_categorie';

    protected $view_livres_empruntes='view_livres_empruntes';

    public function get_list()
    {
        return $this->db->select('*')
            ->from($this->view_livres_categorie)
            ->get()
            ->result();
    }
    public function get_livres_dispo()
    {
        return $this->db->select('*')
            ->from($this->view_livres_categorie)
            ->where('ETAT!=',0)
            ->get()
            ->result();
    }
    public function get_livres_empruntees()
    {
        return $this->db->select('*')
            ->from($this->view_livres_empruntes)
            ->get()
            ->result();
    }

    public function get_list_by_idcategorie($id)
    {
        $this->db->select('*');
        $this->db->from($this->view_livres_categorie);
        $this->db->where('IDCATEGORIE', (int) $id);
        $query = $this->db->get();
        return $query->result();
    }

    public function get_livre_similaire($id){
        $sql="SELECT distinct l.* FROM view_livres_categorie l join livre_motcle lm on l.BOOKID=lm.BOOKID where lm.IDMOCLEES in (select b.IDMOCLEES from livre_motcle b where b.BOOKID=".$id.")";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function get_livres_empruntes_by_userid($id){
        $this->db->select('*');
        $this->db->from($this->view_livres_empruntes);
        $this->db->where('UTILISATEURSID', (int) $id);
        $query = $this->db->get();
        return $query->result();
    }

    function insert($data)
    {
        return $this->db
            ->insert($this->table, $data);
    }


    //compter toutes les livres
    public function count_livres()
    {
        $sql="select * from livres";
        $query = $this->db->query($sql);
        return $query->num_rows();
    }

    //compter toutes les livres dispo
    public function ccount_livres_dispo()
    {
        $sql="select * from livres where ETAT=1";
        $query = $this->db->query($sql);
        return $query->num_rows();
    }

     //compter toutes les livres
    public function ccount_livres_empruntes()
    {
        $sql="select * from livres where ETAT=0";
        $query = $this->db->query($sql);
        return $query->num_rows();
    }

     //compter  les livres PAR utilisateur
    public function count_livres_by_user($id)
    {
        $this->db->select('*');
        $this->db->from($this->view_livres_empruntes);
        $this->db->where('UTILISATEURSID', (int) $id);
        $query = $this->db->get();
        return $query->num_rows();
    }


    public function get_mot_cles()
    {
        $sql="select * from mot_cles";
        $query = $this->db->query($sql);
        return $query->result();
    }


    public function retour_livre_upd($bookid)
    {
        $this->db->where('BOOKID', $bookid);
        $this->db->update('livres', array('ETAT' => 1));
    }
    public function retour_livre_del($iduser,$bookid)
    {
        $this->db->where('UTILISATEURSID', $iduser);
        $this->db->where('BOOKID', $bookid);
        $this->db->delete('emprunts');
    }

    public function get_categorie(){

        $sql="select * from categorie";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function add_livres($data)
    {
        $this->db
            ->insert($this->table, $data);
        $last_id = $this->db->insert_id();
        return $last_id;
    }

    public function add_mot_cles($idlivres,$array){

        foreach( $array as $key=>$value )
        {
            $this->db
                ->insert($this->table_mot_cles, Array('BOOKID'=>$idlivres , 'IDMOCLEES'=>$value));
        }
    }
} 