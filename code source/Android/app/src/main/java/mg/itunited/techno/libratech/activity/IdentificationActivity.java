package mg.itunited.techno.libratech.activity;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import mg.itunited.techno.libratech.R;
import mg.itunited.techno.libratech.fragment.identification.LoginFragment;
import mg.itunited.techno.libratech.fragment.identification.RegisterFragment;

public class IdentificationActivity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_identification);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        displayView(0);
    }



    public void  displayView(int position){

        Fragment fragment= null;

        switch(position){

            case 0:
                   fragment = new LoginFragment();
                break;
            case 1:
                   fragment = new RegisterFragment();
                break;
            default:
                break;
        }

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.flIdentification, fragment);
        fragmentTransaction.commit();

    }

}
