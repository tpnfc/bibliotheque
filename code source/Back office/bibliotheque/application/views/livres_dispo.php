<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Livres disponibles</title>
    <!-- BOOTSTRAP STYLES-->
    <?php
    echo load_css('bootstrap');
    echo load_css('custom');

    echo load_plugin_css('dataTables','.bootstrap');
    ?>

    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
</head>
<body>



<div id="wrapper">

    <!-- /. NAV TOP  -->

    <?php include(__DIR__.'/inc/nav-top.php') ?>

    <!-- /. NAV SIDE  -->
    <?php include(__DIR__.'/inc/nav-side.php') ?>

    <div id="page-wrapper" >
        <div id="page-inner">
            <div class="row">
                <div class="col-md-12">
                    <h2>Livres disponibles </h2>





                    <table id="tab_livres" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>Titre</th>
                            <th>Catégorie</th>
                            <th>Editeur</th>
                            <th>Auteur</th>

                        </tr>
                        </thead>

                        <tbody>
                        <?php
                        foreach($livres as $row)
                        {


                        echo '<tr>';
                        echo '<td>'.$row->TITRE. '</td>';
                        echo '<td>'.$row->INTITULECATEGORIE. '</td>';
                        echo '<td>'.$row->EDITEUR . '</td>';
                        echo '<td>'.$row->AUTEUR . '</td>';

                        ?>


                       <?php

                            echo '</tr>';
                            echo '</tr>';
                            }

                            echo '<br>';
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /. ROW  -->
            <hr />

            <!-- /. ROW  -->
        </div>
        <!-- /. PAGE INNER  -->
    </div>
    <!-- /. PAGE WRAPPER  -->
</div>
<div class="footer">


    <div class="row">
        <div class="col-lg-12" >
            &copy;  2014 yourdomain.com | Design by: <a href="http://binarytheme.com" style="color:#fff;"  target="_blank">www.binarytheme.com</a>
        </div>
    </div>
</div>



<?php

echo load_js("jquery-1.10.2");
echo load_js("bootstrap.min");
echo load_js("custom");


echo load_plugin_js('dataTables','.min');
echo load_plugin_js('dataTables','.bootstrap');
?>
<script>
    $(document).ready(function(){

        $('#tab_livres').dataTable( {
            "language": {
                "lengthMenu": "Affichage _MENU_  par page",
                "zeroRecords": "Aucune donnée a afficher",
                "info": "page _PAGE_ de _PAGES_",
                "infoEmpty": "Aucune donnee",
                "search":"Recherche",
                "infoFiltered": "(filtered from _MAX_ total records)"
            }
        });
    });
</script>

</body>
</html>
